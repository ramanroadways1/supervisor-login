<?php
include("_header.php");
?>
<script>
$(function() {
		$("#own_tno").autocomplete({
		source: '../diary/autofill/get_own_vehicle.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#own_tno').val(ui.item.value);   
            $('#wheeler').val(ui.item.wheeler); 

			// if(ui.item.wheeler=='0')
			// {
				// Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Update wheeler first !</font>',});
				// $('#add_btn').attr('disabled',true);
				// $('#add_btn').hide();
			// }
			// else
			// {
				// $('#add_btn').attr('disabled',false);
				// $('#add_btn').show();
			// }
			
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#own_tno').val("");   
			$('#wheeler').val("");   
			$('#add_btn').attr('disabled',true);
			$('#add_btn').hide();
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Vehicle does not exists.</font>',});
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
</script>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Update Average Data : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				<div class="col-md-12">
					<div class="row">		

						<div class="form-group col-md-3">
							<label>Vehicle Number <sup><font color="red">*</font></sup></label>
							<input id="own_tno" name="own_tno" autocomplete="off" required="required" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');" 
							type="text" class="form-control" />
						</div>
						
						<div class="form-group col-md-3">
							<label>Wheeler <sup><font color="red">*</font></sup></label>
							<input id="wheeler" name="wheeler" autocomplete="off" readonly type="text" class="form-control" />
						</div>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" onclick="SearchVehicle()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn">
							<i id="search_icon" class="fa fa-search" aria-hidden="true"></i> 
							<i id="spinner_icon" style="display:none" class="fa fa-spinner fa-spin" aria-hidden="true"></i> &nbsp; Search</button>
						</div>
						
					<div style="overflow:auto" id="result_div" class="table-responsive form-group col-md-12">
					</div>					
				</div> 
				</div> 
                </div><!-- /.box-body --> 
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("_footer.php") ?>

<div id="func_result"></div>  

<script>
function SearchVehicle()
{
	var tno = $('#own_tno').val();
	var wheeler = $('#wheeler').val();
	
	if(tno=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter vehicle number first !</font>',});
	}
	else
	{
		$('#search_icon').hide();
		$('#spinner_icon').show();
		jQuery.ajax({
				url: "./_load_vehicle_for_avg_update.php",
				data: 'tno=' + tno + '&wheeler=' + wheeler,
				type: "POST",
				success: function(data) {
					$("#result_div").html(data);
					$('#search_icon').show();
					$('#spinner_icon').hide();
				},
				error: function() {}
		});
	}
}

function SaveData()
{
	var tno = $('#own_tno').val();
	var wheeler = $('#wheeler').val();
	var normal_empty_avg = $('#normal_empty_avg').val();
	var normal_normal_avg = $('#normal_normal_avg').val();
	var normal_ol_avg = $('#normal_ol_avg').val();
	var con_empty_avg = $('#con_empty_avg').val();
	var con_normal_avg = $('#con_normal_avg').val();
	var con_ol_avg = $('#con_ol_avg').val();
	
	$('#update_btn').attr('disabled',true);
	$('#loadicon').show();
	jQuery.ajax({
				url: "./save_update_avg_data.php",
				data: 'tno=' + tno + '&wheeler=' + wheeler + '&normal_empty_avg=' + normal_empty_avg + '&normal_normal_avg=' + normal_normal_avg + '&normal_ol_avg=' + normal_ol_avg + '&con_empty_avg=' + con_empty_avg + '&con_normal_avg=' + con_normal_avg + '&con_ol_avg=' + con_ol_avg,
				type: "POST",
				success: function(data) {
					$("#func_result").html(data);
				},
				error: function() {}
		});
}
</script>
