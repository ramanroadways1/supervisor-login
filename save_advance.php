<?php
require_once("./_connect.php");
 
$timestamp = date("Y-m-d H:i:s");
$date = date("Y-m-d");

$trip_id = escapeString($conn,($_POST['trip_id']));
$amount = escapeString($conn,($_POST['amount']));
$branch = escapeString($conn,($_POST['branch']));
$narration = escapeString($conn,($_POST['narration']));

if($trip_id != $_SESSION['advance_trip_id'])
{
	AlertErrorTopRight("Trip not verified !");
	exit();
}

$get_trip = Qry($conn,"SELECT branch,trip_no,driver_code,tno,lr_type,fix_lane FROM dairy.trip WHERE id='$trip_id'");

if(!$get_trip){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($get_trip)==0)
{
	AlertErrorTopRight("Trip not found !");
	exit();
}

$row_trip = fetchArray($get_trip);

$tno = $row_trip['tno'];
$trip_no = $row_trip['trip_no'];
$driver_code = $row_trip['driver_code'];
$fix_lane = $row_trip['fix_lane'];

// check if advance given by branch or not in this trip.

$chk_branch_adv = Qry($conn,"SELECT id FROM dairy.cash WHERE trip_id='$trip_id'");

if(!$chk_branch_adv){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($chk_branch_adv)==0)
{
	AlertErrorTopRight("Only branch can give first advance !");
	exit();
}

if(empty($driver_code))
{
	AlertErrorTopRight("Driver not found !");
	exit();
}

$check_scripts = Qry($conn,"SELECT id FROM dairy.running_scripts WHERE file_name!='LOAD_API_TRANS'");

if(!$check_scripts){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	exit();
}

if(numRows($check_scripts)>0)
{
	AlertErrorTopRight("Please try after some time !");
	exit();
}

$hisab_cache = Qry($conn,"SELECT id FROM dairy.hisab_cache WHERE tno='$tno'");

if(!$hisab_cache){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	exit();
}

if(numRows($hisab_cache)>0)
{
	AlertErrorTopRight("Vehicle hisab in process !");
	exit();
}

$trip_cache = Qry($conn,"SELECT id FROM dairy.trip_cache WHERE tno='$tno'");

if(!$trip_cache){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	exit();
}

if(numRows($trip_cache)>0)
{
	AlertErrorTopRight("Please try after some time !");
	exit();
}

$get_card = Qry($conn,"SELECT card_using,card_assigned FROM dairy.happay_card WHERE tno='$tno'");

if(!$get_card){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	exit();
}

if(numRows($get_card)==0)
{
	AlertErrorTopRight("Card not found !");
	exit();
}
	 
$row_card_using = fetchArray($get_card);

if($row_card_using['card_assigned']=="0")
{
	AlertErrorTopRight("Card not assigned !");
	exit();
}

$card_kit_no = $row_card_using['card_using'];

if($tno != $card_kit_no)
{
	$other_Card_narration = "Vehicle_No : $tno. Card Using : $card_kit_no.";
}
else
{
	$other_Card_narration="";
}
	
$get_card_details = Qry($conn,"SELECT company,card_assigned,card_status as status FROM dairy.happay_card_inventory WHERE veh_no='$card_kit_no'");

if(!$get_card_details){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	exit();
}

if(numRows($get_card_details)==0) 
{
	AlertErrorTopRight("Card not found: $card_kit_no !");
	exit();
}

$row_card_data = fetchArray($get_card_details);

if($row_card_data['status'] != "1")
{
	AlertErrorTopRight("Card is not active !");
	exit();
}

if($row_card_data['card_assigned']!=1)
{
	AlertErrorTopRight("Card not assigned yet !");
	exit();
}

$company = $row_card_data['company'];
$happay_card_balance = $row_card_data['balance']+$amount;

$chk_balance = Qry($conn,"SELECT balance,api_bal FROM dairy.happay_main_balance WHERE company='$company'");

if(!$chk_balance){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	exit();
}

if(numRows($chk_balance)==0)
{
	errorLog("Happay Main Balance not found.",$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	exit();
}
	
$row_main_bal = fetchArray($chk_balance);
	
$happay_main_balance = $row_main_bal['api_bal'];
$happay_main_balance_sys = $row_main_bal['balance'];
	
if($happay_main_balance<$amount)
{
	AlertErrorTopRight("Insufficient balance !");
	exit();
}
	
$updated_main_balance = $happay_main_balance_sys - $amount;
	
$trans_type="HAPPAY";

$get_vou_id = GetVouId("T",$conn,"mk_tdv","tdvid",$branch);

if(!$get_vou_id || $get_vou_id=="0" || $get_vou_id==""){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	exit();
}
	
$tids = $get_vou_id;
	
$trans_id_Qry = GetTxnId_eDiary($conn,$trans_type);

if(!$trans_id_Qry || $trans_id_Qry=="" || $trans_id_Qry=="0"){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while getting txn-id !");
	exit();
}
	
$trans_id = $trans_id_Qry;

StartCommit($conn);
$flag = true;

$insert_cash_diary=Qry($conn,"INSERT INTO dairy.cash (trip_id,trans_id,trans_type,tno,vou_no,amount,date,narration,branch,timestamp) VALUES 
('$trip_id','$trans_id','$trans_type','$tno','$tids','$amount','$date','$narration','$branch','$timestamp')");

if(!$insert_cash_diary){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_happay_trans = Qry($conn,"INSERT INTO dairy.happay_card_transactions(card_no,card_using,driver_code,narration,trans_id,trans_type,credit,
branch,date,superv_entry,timestamp) VALUES ('$tno','$card_kit_no','$driver_code','$other_Card_narration','$trans_id','Wallet Credit',
'$amount','$branch','$date','1','$timestamp')");
	
if(!$insert_happay_trans){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_happay_main_bal = Qry($conn,"UPDATE dairy.happay_main_balance SET balance='$updated_main_balance' WHERE company='$company'");

if(!$update_happay_main_bal){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}			

$insert_vou = Qry($conn,"INSERT INTO mk_tdv(user,company,tdvid,date,newdate,truckno,dname,amt,mode,dest,timestamp) VALUES 
('$branch','$company','$tids','$date','$date','$tno','','$amount','$trans_type','$narration','$timestamp')");	
		
if(!$insert_vou){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$update_trip = Qry($conn,"UPDATE dairy.trip SET cash=cash+'$amount' WHERE id='$trip_id'");

if(!$update_trip){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

if(AffectedRows($conn)==0)
{
	$flag = false;
	errorLog("Trip Balance not updated. Tripid: $trip_id. Amount: $amount. VehNo: $tno.",$conn,$page_name,__LINE__);
}

$select_amount=Qry($conn,"SELECT id,amount_hold FROM dairy.driver_up WHERE down=0 AND code='$driver_code' ORDER BY id DESC LIMIT 1");

if(!$select_amount){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$row_amount = fetchArray($select_amount);
$hold_amt = $row_amount['amount_hold']+$amount;
$driver_balance_id = $row_amount['id'];

$insert_book=Qry($conn,"INSERT INTO dairy.driver_book (driver_code,tno,trip_id,trip_no,trans_id,desct,credit,balance,date,branch,timestamp) 
VALUES ('$driver_code','$tno','$trip_id','$trip_no','$trans_id','CASH_ADV','$amount','$hold_amt','$date','$branch','$timestamp')");

if(!$insert_book){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_hold_amount=Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold+'$amount' WHERE id='$driver_balance_id'");

if(!$update_hold_amount){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

// Insert alert if supervisor crossed the entry or amount limit start here

if($fix_lane!=0)
{
	$chk_count = Qry($conn,"SELECT id FROM dairy.cash WHERE date='$date' AND tno='$tno'");
	
	if(!$chk_count){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$chk_amount = Qry($conn,"SELECT SUM(amount) as total_amount FROM dairy.cash WHERE date='$date' AND tno='$tno'");
	
	if(!$chk_amount){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	// $row_chk_count = fetchArray($chk_count);
	$row_chk_amount = fetchArray($chk_amount);

	$get_count_limit = Qry($conn,"SELECT entry_limit FROM dairy.trip_adv_exp_limit_for_fix WHERE adv_exp='ADV' AND limit_type='2'");
	
	if(!$get_count_limit){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($get_count_limit) > 0)
	{
		$row_entry_limit = fetchArray($get_count_limit);
		
		$total_entries = numRows($chk_count);
		
		if($total_entries > $row_entry_limit['entry_limit'])
		{
			$insert_alert_adv_entry_limit = Qry($conn,"INSERT INTO dairy._alert_fix_adv_exp(tno,driver,amount,entry_no,entry_limit,supervisor,
			timestamp) VALUES ('$tno','$driver_code','$amount','$total_entries','$row_entry_limit[entry_limit]','$supv_id','$timestamp')");
			
			if(!$insert_alert_adv_entry_limit){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
	}
	
	$get_amount_limit = Qry($conn,"SELECT amount FROM dairy.trip_adv_exp_limit_for_fix WHERE adv_exp='ADV' AND limit_type='1'");
	
	if(!$get_amount_limit){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($get_amount_limit) > 0)
	{
		$row_amount_limit = fetchArray($get_amount_limit);
		
		$total_amount = $row_chk_amount['total_amount'];
		
		if($total_amount > $row_amount_limit['amount'])
		{
			$insert_alert_adv_amount_limit = Qry($conn,"INSERT INTO dairy._alert_fix_adv_exp(tno,driver,amount,amount_limit,supervisor,
			timestamp) VALUES ('$tno','$driver_code','$amount','$row_amount_limit[amount]','$supv_id','$timestamp')");
			
			if(!$insert_alert_adv_amount_limit){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
	}
}

// Insert alert if supervisor crossed the entry or amount limit end here

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	AlertRightCornerSuccess("Advance Credited Successfully !");
	
	echo "<script>
			$('#Form1')[0].reset();
			$('#advance_submit').attr('disabled',false);
			$('#adv_modal_close_btn')[0].click();
		</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	exit();
}	
?>