<?php
require_once("_connect.php");

$tno = escapeString($conn,($_POST['tno']));
$model = escapeString($conn,($_POST['model']));

if($tno!='')
{
	$get_avg_data = Qry($conn,"SELECT t.veh_no,o.wheeler,t.max_capacity,t.route_type,t.empty_avg,t.normal_avg,t.overload_avg,t.timestamp,o.trolly_size,
	o.tank_weight,o.tyre_type,o.mfg_yr,o.cng,o.model 
	FROM dairy._avg_data AS t
	LEFT OUTER JOIN dairy.own_truck AS o ON o.tno = t.veh_no
	WHERE t.veh_no='$tno' ORDER BY t.veh_no ASC");
}
else
{
	$get_avg_data = Qry($conn,"SELECT t.veh_no,o.wheeler,t.max_capacity,t.route_type,t.empty_avg,t.normal_avg,t.overload_avg,t.timestamp,o.trolly_size,
	o.tank_weight,o.tyre_type,o.mfg_yr,o.cng,o.model 
	FROM dairy._avg_data AS t
	LEFT OUTER JOIN dairy.own_truck AS o ON o.tno = t.veh_no 
	WHERE t.veh_no IN(SELECT tno from dairy.own_truck WHERE model='$model' AND is_sold!='1') ORDER BY t.veh_no ASC");
}

if(!$get_avg_data){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
?>
  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Vehicle_No</th>
                        <th>Model</th>
                        <th>Wheeler</th>
                        <th>Max_Cap.</th>
                        <th>Trolly_Size</th>
                        <th>Tank_Weight</th>
                        <th>Tyre_Type</th>
                        <th>CNG</th>
                        <th>Route_Type</th>
                        <th>Empty_Avg</th>
                        <th>Normal_Avg</th>
                        <th>Overload_Avg</th>
                        <th>Updated_At</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	
	if(numRows($get_avg_data)==0)
	{
		echo "<tr>
			<td colspan='14'>No user found !</td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		
		while($row = fetchArray($get_avg_data))
		{
			$update_timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			
			if($row['cng']=="1"){
				$cng = "Yes";
			}else{
				$cng = "No";
			}
			
			if($row['mfg_yr']!=""){
				$model = "$row[model]<br>($row[mfg_yr])";
			}else{
				$model = "$row[model]";
			}
			
			echo "<tr>
				<td>$i</td>
				<td>$row[veh_no]</td>
				<td>$model</td>
				<td>$row[wheeler]</td>
				<td>$row[max_capacity]</td>
				<td>$row[trolly_size]</td>
				<td>$row[tank_weight]</td>
				<td>$row[tyre_type]</td>
				<td>$cng</td>
				<td>$row[route_type]</td>
				<td>$row[empty_avg]</td>
				<td>$row[normal_avg]</td>
				<td>$row[overload_avg]</td>
				<td>$timestamp</td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				  
<script>
$(document).ready(function() {
    $('#example1').DataTable();
} );

$('#search_icon').show();
$('#spinner_icon').hide();
</script>				  