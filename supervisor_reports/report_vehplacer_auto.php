<?php
require('connect.php');

$searchTerm = $_REQUEST['term'];

$query = Qry($conn,"SELECT id,username FROM dairy.user WHERE username LIKE '%".$searchTerm."%' ORDER BY username ASC LIMIT 5");

$data = array();

if(!$query){
	echo mysqli_error($conn);
	exit();
}

while ($row = fetchArray($query)) 
{ 
	 $data[] = array("value"=>$row['username'],"label"=>$row['username'], "dbid"=>$row['id']);
}

echo json_encode($data);
?>