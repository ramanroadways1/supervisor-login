<?php require('header.php'); 
?>
  
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<style type="text/css">
  .applyBtn{
    border-radius: 0px !important;
  }
  .show-calendar{
    top: 180px !important;
  } 
    .applyBtn{
        border-radius: 0px !important;
    }
    table.table-bordered.dataTable td {
        padding: 10px 5px 10px 10px;
    }
     .dt-buttons{float: right;}
    .user_data_filter{
        float: right;
    }

    .dt-button {
        padding: 5px 20px;
        text-transform: uppercase;
        font-size: 12px;
        text-align: center;
        cursor: pointer;
        outline: none;
        color: #fff;
        background-color: #37474f ;
        border: none;
        border-radius:  2px;
        box-shadow: 0 4px #999;
    }

    .dt-button:hover {background-color: #3e8e41}

    .dt-button:active {
        background-color: #3e8e41;
        box-shadow: 0 5px #666;
        transform: translateY(4px);
    }
    #user_data_wrapper{
        width: 100% !important;
    }
    .dt-buttons{
        margin-bottom: 20px;
    }


#appenddiv1, #appenddiv2, #appenddiv3, #appenddiv4 {
    display: block; 
    position:relative
} 
.ui-autocomplete {
    position: absolute;
}
 
.table-hover tbody tr:hover td,.table-hover tbody tr:hover th{background-color:#ffedda}.table td{vertical-align:middle!important;font-size:11px!important;color:#000;font-family:Verdana,Geneva,sans-serif;padding-top:4px;padding-right:4px;padding-bottom:4px;padding-left:10px}.table-bordered td{border:3px solid #e3e6f0}#user_data_info,#user_data_length{float:left}#user_data_filter,#user_data_paginate{float:right}.paginate_button{color:#000;float:left;padding:6px 12px;text-decoration:none;border:1px solid #ccc;cursor:pointer}.ellipsis{display:none}[type=search]{margin-right:10px; width: 250px; }.ui-autocomplete{z-index:2150000000!important}.container input{position:absolute;opacity:0;cursor:pointer;height:0;width:0}.checkmark{border-radius:2px;position:absolute;top:0;height:20px;width:20px;background-color:#fff;border:1px solid #000}.container:hover input~.checkmark{background-color:#fff}.container input:checked~.checkmark{background-color:#fff}.container input:disabled~.checkmark{background-color:#eaecf4}.checkmark:after{content:"";position:absolute;display:none}.container input:checked~.checkmark:after{display:block}.container .checkmark:after{left:6px;top:-1px;width:8px;height:16px;border:solid #000;border-width:0 3px 3px 0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}button:disabled,button[disabled]{border:1px solid #333!important;color:#333!important;cursor:no-drop} .table .thead-light th{text-align: center; font-size: 11px; color:#444;} .component{display: none;} 
  table {width: 100% !important;} table.table-bordered.dataTable td { white-space: nowrap; overflow: hidden; text-overflow:ellipsis;  }
  .table .thead-light th{
    text-transform: none !important;
  }
</style>
<script type="text/javascript"> 

  $(function() {
    $("#broker").autocomplete({
    source: 'report_broker_auto.php',
    appendTo: '#appenddiv1',
    select: function (event, ui) { 
        $('#broker').val(ui.item.value);   
        return false;
    },
    change: function (event, ui) {
    if(!ui.item){
        $(event.target).val("");
      Swal.fire({
      icon: 'error',
      title: 'Error !!!',
      text: 'Broker does not exists !'
      })
      $("#broker").val("");
      $("#broker").focus();
    }
    }, 
    focus: function (event, ui){
    return false;
    }
    });
  });

  $(function() {
    $("#billname").autocomplete({
    source: 'report_billname_auto.php',
    appendTo: '#appenddiv2',
    select: function (event, ui) { 
            $('#billname').val(ui.item.value);   
            return false;
    },
    change: function (event, ui) {
    if(!ui.item){
        $(event.target).val("");
      Swal.fire({
      icon: 'error',
      title: 'Error !!!',
      text: 'Bill Party does not exists !'
      })
      $("#billname").val("");
      $("#billname").focus();
    }
    }, 
    focus: function (event, ui){
    return false;
    }
    });
  });
 
  $(function() {
    $("#vehplacer").autocomplete({
    source: 'report_vehplacer_auto.php',
    appendTo: '#appenddiv3',
    select: function (event, ui) { 
            $('#vehplacer').val(ui.item.value);   
            return false;
    },
    change: function (event, ui) {
    if(!ui.item){
        $(event.target).val("");
      Swal.fire({
      icon: 'error',
      title: 'Error !!!',
      text: 'Vehicle Placer does not exists !'
      })
      $("#vehplacer").val("");
      $("#vehplacer").focus();
    }
    }, 
    focus: function (event, ui){
    return false;
    }
    });
  });

  $(function() {
    $("#truck").autocomplete({
    source: 'report_truck_auto.php',
    appendTo: '#appenddiv4',
    select: function (event, ui) { 
            $('#truck').val(ui.item.value);   
            return false;
    },
    change: function (event, ui) {
    if(!ui.item){
        $(event.target).val("");
      Swal.fire({
      icon: 'error',
      title: 'Error !!!',
      text: 'Truck does not exists !'
      })
      $("#truck").val("");
      $("#truck").focus();
    }
    }, 
    focus: function (event, ui){
    return false;
    }
    });
  });

</script>
 <div class="col-md-12"> <h3> Bilty Summary</h3> </div>
  
 <div id="response"></div>
  
<form method="post" action="" id="getPAGE" autocomplete="off"> 
<div class="col-md-12" >
<div class="card-body "  style="background-color: #fff; border: 1px solid #ccc;">
<div class="row">

    <div class="col-md-2 ">
      <label style="text-transform: uppercase;"> Report TYPE </label>
      <select id="seltype" class="form-control" name="seltype"  onChange="updt(this);" required="required">
        <option value=""> -- select -- </option>
        <option value="report1"> Supervisor Wise </option>
        <option value="report2"> Pending POD  </option>

        <option value="report10"> Broker Wise </option> 
        <option value="report11"> Billing Party Wise </option> 
        <option value="report12"> Vehicle Placer Wise </option> 
        <option value="report13"> Truck Wise </option> 

      </select>
    </div>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <style type="text/css">
      .show-calendar{
        top: 250px !important;
      }
    </style>

    <div class="col-md-2">
      <label style="text-transform: uppercase;"> SUPERVISOR </label>
      <select class="form-control" name="branch" id="branch"  disabled="">
        <option value=""> -- select -- </option> 
        <?php
		$get_supervisor = Qry($conn,"SELECT id,title,username FROM dairy.user WHERE role='1' ORDER BY username ASC");
		
		if(!$get_supervisor){
			errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			exit();
		}
		
		if(numRows($get_supervisor)>0)
		{
			while($row_s = fetchArray($get_supervisor))
			{
				echo "<option value='$row_s[id]'>$row_s[title]</option>";
			}
		}
		?>
      </select>
    </div>  
  
    <div  class="col-md-2">
      <label>BALANCE</label>
      <select name="bal" id="bal" class="form-control" disabled="">
        <option value="1">All </option>
        <option value="0">Pending </option>
      </select>
    </div>

    <div class="col-md-2">
      <label style="text-transform: uppercase;">DATE RANGE</label>
      <input type="text" name="daterange" class="form-control" value="" id="dt"  style="min-height: 38px;" />
      <input type="hidden" id="fromdate" name="fromdate">
      <input type="hidden" id="todate" name="todate"> 
    </div>
  

  <div class="col-md-3" id="report10" style="display: none;">
            <label> Broker Name </label>
            <input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="text" class="form-control" id="broker" name="broker" value="" style="font-size: 18px; text-transform: uppercase;" disabled="" />
            <div id="appenddiv1"></div>
    </div> 
  <div class="col-md-3" id="report11" style="display: none;">
            <label> Billing Party </label>
            <input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="text" class="form-control" id="billname" name="billname" value="" style="font-size: 18px; text-transform: uppercase;" disabled="" />
            <div id="appenddiv2"></div>
    </div> 
  <div class="col-md-3" id="report12" style="display: none;">
            <label> Vehicle Placer </label>
            <input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="text" class="form-control" id="vehplacer" name="vehplacer" value="" style="font-size: 18px; text-transform: uppercase;" disabled="" />
            <div id="appenddiv3"></div>
    </div> 
  <div class="col-md-3" id="report13" style="display: none;">
            <label> Truck No </label>
            <input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="text" class="form-control" id="truck" name="truck" value="" style="font-size: 18px; text-transform: uppercase;" disabled="" />
            <div id="appenddiv4"></div>
    </div> 
 
<script type="text/javascript">
  $(function() {
      $('input[name="daterange"]').daterangepicker({
        // minDate: '2019-09-15',
        opens: 'left'
      }, function(start, end, label) {
        document.getElementById('fromdate').value=start.format('YYYY-MM-DD');
        document.getElementById('todate').value=end.format('YYYY-MM-DD'); 
        console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
      });
  });
</script>

    <div class="col-md-1" style="top:-5px !important;"> 
      <label style=""> </label> <br>
      <button type="submit" class='btn btn-success'> <i class='fa fa-search '></i> <b>  </b> </button>    
      
    </div>
</div>
</div>
</form> 
</div>
<style type="text/css">
     body {
    overflow-y: scroll !important;
}
</style>
<div id="getPAGEDIV" class="col-md-12" style="margin-top: 25px !important;"></div>


<script type="text/javascript">
  function updt(sel) { 

      if(sel.value == "report1" || sel.value == "report2" || sel.value == "report3")
      {
        $("#branch").prop('required',true);
        $("#branch").prop('disabled',false);        

      } else {
        $("#branch").prop('required',false);
        $("#branch").prop('disabled',true); 
      }

        if(sel.value == "report10") 
        {
          $("#report10").css("display", "block");
          $("#broker").prop('required',true);
          $("#broker").prop('disabled',false);
          $("#bal").prop('required',true);
          $("#bal").prop('disabled',false); 
        } else {
          $("#report10").css("display", "none");
          $("#broker").prop('required',false);
          $("#broker").prop('disabled',true);
          $("#bal").prop('required',false);
          $("#bal").prop('disabled',true);
        }

        if(sel.value == "report11") 
        {
          $("#report11").css("display", "block");
          $("#billname").prop('required',true);
          $("#billname").prop('disabled',false);
          $("#bal").prop('required',true);
          $("#bal").prop('disabled',false); 
        } else {
          $("#report11").css("display", "none");
          $("#billname").prop('required',false);
          $("#billname").prop('disabled',true);
        }

        if(sel.value == "report12") 
        {
          $("#report12").css("display", "block");
          $("#vehplacer").prop('required',true);
          $("#vehplacer").prop('disabled',false);
          $("#bal").prop('required',true);
          $("#bal").prop('disabled',false); 
        } else {
          $("#report12").css("display", "none");
          $("#vehplacer").prop('required',false);
          $("#vehplacer").prop('disabled',true);
        }

        if(sel.value == "report13") 
        {
          $("#report13").css("display", "block");
          $("#truck").prop('required',true);
          $("#truck").prop('disabled',false);
          $("#bal").prop('required',true);
          $("#bal").prop('disabled',false); 
        } else {
          $("#report13").css("display", "none");
          $("#truck").prop('required',false);
          $("#truck").prop('disabled',true);
        }
  } 
 
  $(document).on('submit', '#getPAGE', function()
    {   
      var data = $(this).serialize(); 
      $.ajax({  
        type : 'POST',
        url  : 'report_fetch.php',
        data : data,
        success: function(data) {       
        $('#getPAGEDIV').html(data);  
        }
      });
      return false;  
   });
</script>
 <?php include('footer.php'); ?>