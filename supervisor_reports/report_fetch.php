<?php
 
	require('connect.php');

    $seltype = $conn -> real_escape_string($_POST['seltype']); 
    
  	if($seltype=="report1"){

		$branch = $conn -> real_escape_string($_POST['branch']); 
    	$daterange = $conn -> real_escape_string($_POST['daterange']);
		function before ($thiss, $inthat)
		{
		    return substr($inthat, 0, strpos($inthat, $thiss));
		}

		function after ($thiss, $inthat)
		{
		    if (!is_bool(strpos($inthat, $thiss)))
		        return substr($inthat, strpos($inthat,$thiss)+strlen($thiss));
		}
		 
		$fromdate = before('-', $daterange); 
		$fromdate = strtotime($fromdate); 
		$todate = after('-', $daterange); 
		$todate = strtotime($todate);

    	echo '
    	<div class="card-body table-responsive"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
					<th>System_Date</th>
					<th>LR_Date</th>
					<th>Company</th>
					<th>Branch</th>
					<th>Bilty_No</th>
					<th>Bill_Type</th>
					<th>LR_By</th>
					<th>Placed_By</th>
					<th>LR_No</th> 
					<th>Broker</th>
					<th>Billing_Party</th>
					<th>Truck_No</th>
					<th>From</th>
					<th>To</th>
					<th>Actual_Weight</th>
					<th>Charge_Weight</th>
					<th>Rate</th>
					<th>Freight</th>
					<th>Received</th>
					<th>Balance</th>
					<th>POD_Upload</th>
					<th>POD_Branch</th>
					<th>POD_Date</th> 
					<th>Bill_No</th>
					<th>Bill_Amount</th> 
					<th>Bill_Weight</th>
					<th>Bill_Gen_Date</th> 
					<th>Billing_Branch</th> 
					<th>Bill_Require</th> 
				</tr>
		      </thead> 
		 	</table>
		</div>
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sAjaxSource": "report_data.php?p='.$branch.'&f='.$fromdate.'&t='.$todate.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
		"dom": "lBfrtip",
		"ordering": true,
		"buttons": [
		"copy", "csv", "excel", "print"
		],
		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		// "targets":[4],
		// "orderable":false,
		},
		],
		"aoColumns": [
		{ mData: "0" },
		{ mData: "1" },
		{ mData: "2" },
		{ mData: "3" },
		{ mData: "4" },
		{ mData: "5" },
		{ mData: "6" },
		{ mData: "7" },
		{ mData: "8" },
		{ mData: "9" },
		{ mData: "10" },
		{ mData: "11" },
		{ mData: "12" },
		{ mData: "13" },
		{ mData: "14" },
		{ mData: "15" },
		{ mData: "16" },
		{ mData: "17" },
		{ mData: "18" },
		{ mData: "19" },
		{ mData: "20" },
		{ mData: "21" },
		{ mData: "22" },
		{ mData: "23" },
		{ mData: "24" },
		{ mData: "25" },
		{ mData: "26" },
		{ mData: "27" },
		{ mData: "28" }
		],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  

		}); 
     
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		} ); </script>';
 
  	} else if($seltype=="report2"){

		$branch = $conn -> real_escape_string($_POST['branch']); 
    	$daterange = $conn -> real_escape_string($_POST['daterange']);
		function before ($thiss, $inthat)
		{
		    return substr($inthat, 0, strpos($inthat, $thiss));
		}

		function after ($thiss, $inthat)
		{
		    if (!is_bool(strpos($inthat, $thiss)))
		        return substr($inthat, strpos($inthat,$thiss)+strlen($thiss));
		}
		 
		$fromdate = before('-', $daterange); 
		$fromdate = strtotime($fromdate); 
		$todate = after('-', $daterange); 
		$todate = strtotime($todate);

    	echo '
    	<div class="card-body table-responsive"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
					<th>System_Date</th>
					<th>LR_Date</th>
					<th>Company</th>
					<th>Branch</th>
					<th>Bilty_No</th>
					<th>Bill_Type</th>
					<th>LR_By</th>
					<th>Placed_By</th>
					<th>LR_No</th> 
					<th>Broker</th>
					<th>Billing_Party</th>
					<th>Truck_No</th>
					<th>From</th>
					<th>To</th>
					<th>Actual_Weight</th>
					<th>Charge_Weight</th>
					<th>Rate</th>
					<th>Freight</th>
					<th>Received</th>
					<th>Balance</th>
					<th>POD_Upload</th>
					<th>POD_Branch</th>
					<th>POD_Date</th> 
					<th>Bill_No</th>
					<th>Bill_Amount</th> 
					<th>Bill_Weight</th>
					<th>Bill_Gen_Date</th> 
					<th>Billing_Branch</th> 
					<th>Bill_Require</th> 
				</tr>
		      </thead> 
		 	</table>
		</div>
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sAjaxSource": "report_data1.php?p='.$branch.'&f='.$fromdate.'&t='.$todate.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
		"dom": "lBfrtip",
		"ordering": true,
		"buttons": [
		"copy", "csv", "excel", "print"
		],
		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		// "targets":[4],
		// "orderable":false,
		},
		],
		"aoColumns": [
		{ mData: "0" },
		{ mData: "1" },
		{ mData: "2" },
		{ mData: "3" },
		{ mData: "4" },
		{ mData: "5" },
		{ mData: "6" },
		{ mData: "7" },
		{ mData: "8" },
		{ mData: "9" },
		{ mData: "10" },
		{ mData: "11" },
		{ mData: "12" },
		{ mData: "13" },
		{ mData: "14" },
		{ mData: "15" },
		{ mData: "16" },
		{ mData: "17" },
		{ mData: "18" },
		{ mData: "19" },
		{ mData: "20" },
		{ mData: "21" },
		{ mData: "22" },
		{ mData: "23" },
		{ mData: "24" },
		{ mData: "25" },
		{ mData: "26" },
		{ mData: "27" },
		{ mData: "28" }
		],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  

		}); 
     
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		} ); </script>';

// 		$branch = $conn -> real_escape_string($_POST['branch']); 
//     	$daterange = $conn -> real_escape_string($_POST['daterange']);
// 		function before ($thiss, $inthat)
// 		{
// 		    return substr($inthat, 0, strpos($inthat, $thiss));
// 		}

// 		function after ($thiss, $inthat)
// 		{
// 		    if (!is_bool(strpos($inthat, $thiss)))
// 		        return substr($inthat, strpos($inthat,$thiss)+strlen($thiss));
// 		}
		 
// 		$fromdate = before('-', $daterange); 
// 		$fromdate = strtotime($fromdate); 
// 		$todate = after('-', $daterange); 
// 		$todate = strtotime($todate);

// $date1 = date_create(before('-', $daterange));
// $date2 = date_create(after('-', $daterange));
// $diff = date_diff($date1,$date2);
// // if($diff->format("%a")>31){

// // 		echo "
// // 		<script>
// // 		Swal.fire({
// // 		icon: 'error',
// // 		title: 'Error !!!',
// // 		text: 'Max difference should not be greater than 31 days'
// // 		})
// // 		</script>";	
// // 		exit();
// // }

//     	echo '
//     	<div class="card-body table-responsive"> 
// 		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
// 		      <thead class="thead-light">
// 		        <tr>
// 					<th>System_Date</th>
// 					<th>LR_Date</th>
// 					<th>Company</th>
// 					<th>Branch</th>
// 					<th>Bilty_No</th>
// 					<th>Bill_Type</th>
// 					<th>LR_By</th>
// 					<th>Veh_PlacedBy</th>
// 					<th>LR_No</th> 
// 					<th>Broker</th>
// 					<th>Billing_Party</th>
// 					<th>Truck_No</th>
// 					<th>From</th>
// 					<th>To</th> 
// 					<th>Wheeler</th>
// 					<th>Max_Weight</th>
// 					<th>Actual_Weight</th>
// 					<th>Charge_Weight</th>
// 					<th>Rate</th>
// 					<th>Freight</th>
// 					<th>Received</th>
// 					<th>Balance</th>
// 					<th>POD_Upload</th>
// 					<th>POD_Branch</th>
// 					<th>POD_Date</th>
// 					<th>Bill_No</th>
// 					<th>Bill_Amount</th> 
// 					<th>Bill_Weight</th>
// 					<th>Bill_Gen_Date</th> 
// 					<th>Billing_Branch</th> 
// 					<th>Bill_Require</th> 
// 				</tr>
// 		      </thead> 
// 		 	</table>
// 		</div>
 
// 		<script type="text/javascript">
// 		jQuery( document ).ready(function() {

// 		$("#loadicon").show(); 
// 		var table = jQuery("#user_data").dataTable({
// 		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
// 		"bProcessing": true,
// 		"sAjaxSource": "report_wt_data.php?p='.$branch.'&f='.$fromdate.'&t='.$todate.'",
// 		"bPaginate": true,
// 		"sPaginationType":"full_numbers",
// 		"iDisplayLength": 10,
// 		"dom": "lBfrtip",
// 		"ordering": true,
// 		"buttons": [
// 		"copy", "csv", "excel", "print"
// 		],
// 		//"order": [[ 8, "desc" ]],
// 		"columnDefs":[
// 		{
// 		// "targets":[4],
// 		// "orderable":false,
// 		},
// 		],
// 		"aoColumns": [
// 		{ mData: "0" },
// 		{ mData: "1" },
// 		{ mData: "2" },
// 		{ mData: "3" },
// 		{ mData: "4" },
// 		{ mData: "5" },
// 		{ mData: "6" },
// 		{ mData: "7" },
// 		{ mData: "8" },
// 		{ mData: "9" },
// 		{ mData: "10" },
// 		{ mData: "11" },
// 		{ mData: "12" },
// 		{ mData: "13" },
// 		{ mData: "14" },
// 		{ mData: "15" },
// 		{ mData: "16" },
// 		{ mData: "17" },
// 		{ mData: "18" },
// 		{ mData: "19" },
// 		{ mData: "20" },
// 		{ mData: "21" },
// 		{ mData: "22" },
// 		{ mData: "23" },
// 		{ mData: "24" },
// 		{ mData: "25" },
// 		{ mData: "26" },
// 		{ mData: "27" },
// 		{ mData: "28" },
// 		{ mData: "29" },
// 		{ mData: "30" }
// 		],
// 		"initComplete": function( settings, json ) {
// 		$("#loadicon").hide();
// 		}
// 		});  

// 		}); 
     
// 		$(document).ready(function() { 
// 		var table = $("#user_data").DataTable(); 
// 		} ); </script>';
 
  	} else if($seltype=="report3"){

		$branch = $conn -> real_escape_string($_POST['branch']); 
    	$daterange = $conn -> real_escape_string($_POST['daterange']);
		function before ($thiss, $inthat)
		{
		    return substr($inthat, 0, strpos($inthat, $thiss));
		}

		function after ($thiss, $inthat)
		{
		    if (!is_bool(strpos($inthat, $thiss)))
		        return substr($inthat, strpos($inthat,$thiss)+strlen($thiss));
		}
		 
		$fromdate = before('-', $daterange); 
		$fromdate = strtotime($fromdate); 
		$todate = after('-', $daterange); 
		$todate = strtotime($todate);

$date1 = date_create(before('-', $daterange));
$date2 = date_create(after('-', $daterange));
$diff = date_diff($date1,$date2);
if($diff->format("%a")>31){

		echo "
		<script>
		Swal.fire({
		icon: 'error',
		title: 'Error !!!',
		text: 'Max difference should not be greater than 31 days'
		})
		</script>";	
		exit();
}
    	echo '
    	<div class="card-body table-responsive"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
					<th>System_Date</th>
					<th>LR_Date</th>
					<th>Company</th>
					<th>Branch</th>
					<th>Bilty_No</th>
					<th>Bill_Type</th>
					<th>LR_By</th>
					<th>Vehicle_By</th>
					<th>LR_No</th> 
					<th>Broker</th>
					<th>Billing_Party</th>
					<th>Truck_No</th>
					<th>From</th>
					<th>To</th>
					<th>Actual_Weight</th>
					<th>Charge_Weight</th>
					<th>Rate</th>
					<th>Freight</th>
					<th>Received</th>
					<th>Balance</th>
					<th>POD_Upload</th>
					<th>POD_Branch</th>
					<th>POD_Date</th>
					<th>Expense</th>
					<th>Exp_Amount</th>
					<th>Bill_No</th>
					<th>Bill_Amount</th> 
					<th>Bill_Weight</th>
					<th>Bill_Gen_Date</th> 
					<th>Billing_Branch</th> 
					<th>Bill_Require</th> 
				</tr>
		      </thead> 
		 	</table>
		</div>
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sAjaxSource": "report_exp_data.php?p='.$branch.'&f='.$fromdate.'&t='.$todate.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
		"dom": "lBfrtip",
		"ordering": true,
		"buttons": [
		"copy", "csv", "excel", "print"
		],
		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		// "targets":[4],
		// "orderable":false,
		},
		],
		"aoColumns": [
		{ mData: "0" },
		{ mData: "1" },
		{ mData: "2" },
		{ mData: "3" },
		{ mData: "4" },
		{ mData: "5" },
		{ mData: "6" },
		{ mData: "7" },
		{ mData: "8" },
		{ mData: "9" },
		{ mData: "10" },
		{ mData: "11" },
		{ mData: "12" },
		{ mData: "13" },
		{ mData: "14" },
		{ mData: "15" },
		{ mData: "16" },
		{ mData: "17" },
		{ mData: "18" },
		{ mData: "19" },
		{ mData: "20" },
		{ mData: "21" },
		{ mData: "22" },
		{ mData: "23" },
		{ mData: "24" },
		{ mData: "25" },
		{ mData: "26" },
		{ mData: "27" },
		{ mData: "28" },
		{ mData: "29" },
		{ mData: "30" }
		],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  

		}); 
     
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		} ); </script>';
 
  	} else if($seltype=="report10"){

		$bal = $conn -> real_escape_string($_POST['bal']);  
		$broker = $conn -> real_escape_string($_POST['broker']);  
		$broker = str_replace('=', '_', base64_encode($broker));
    	$daterange = $conn -> real_escape_string($_POST['daterange']);
		function before ($thiss, $inthat)
		{
		    return substr($inthat, 0, strpos($inthat, $thiss));
		}

		function after ($thiss, $inthat)
		{
		    if (!is_bool(strpos($inthat, $thiss)))
		        return substr($inthat, strpos($inthat,$thiss)+strlen($thiss));
		}
		 
		$fromdate = before('-', $daterange); 
		$fromdate = strtotime($fromdate); 
		$todate = after('-', $daterange); 
		$todate = strtotime($todate);

    	echo '
    	<div class="card-body table-responsive"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
					<th>System_Date</th>
					<th>LR_Date</th>
					<th>Company</th>
					<th>Branch</th>
					<th>Bilty_No</th>
					<th>Bill_Type</th>
					<th>LR_By</th>
					<th>Veh_PlacedBy</th>
					<th>LR_No</th> 
					<th>Broker</th>
					<th>Billing_Party</th>
					<th>Truck_No</th>
					<th>From</th>
					<th>To</th> 
					<th>Wheeler</th>
					<th>Max_Weight</th>
					<th>Actual_Weight</th>
					<th>Charge_Weight</th>
					<th>Rate</th>
					<th>Freight</th>
					<th>Received</th>
					<th>Balance</th>
					<th>POD_Upload</th>
					<th>POD_Branch</th>
					<th>POD_Date</th>
					<th>Bill_No</th>
					<th>Bill_Amount</th> 
					<th>Bill_Weight</th>
					<th>Bill_Gen_Date</th> 
					<th>Billing_Branch</th> 
					<th>Bill_Require</th> 
				</tr>
		      </thead> 
		 	</table>
		</div>
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sAjaxSource": "report_broker_data.php?p='.$broker.'&f='.$fromdate.'&t='.$todate.'&b='.$bal.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
		"dom": "lBfrtip",
		"ordering": true,
		"buttons": [
		"copy", "csv", "excel", "print"
		],
		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		// "targets":[4],
		// "orderable":false,
		},
		],
		"aoColumns": [
		{ mData: "0" },
		{ mData: "1" },
		{ mData: "2" },
		{ mData: "3" },
		{ mData: "4" },
		{ mData: "5" },
		{ mData: "6" },
		{ mData: "7" },
		{ mData: "8" },
		{ mData: "9" },
		{ mData: "10" },
		{ mData: "11" },
		{ mData: "12" },
		{ mData: "13" },
		{ mData: "14" },
		{ mData: "15" },
		{ mData: "16" },
		{ mData: "17" },
		{ mData: "18" },
		{ mData: "19" },
		{ mData: "20" },
		{ mData: "21" },
		{ mData: "22" },
		{ mData: "23" },
		{ mData: "24" },
		{ mData: "25" },
		{ mData: "26" },
		{ mData: "27" },
		{ mData: "28" },
		{ mData: "29" },
		{ mData: "30" }
		],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  

		}); 
     
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		} ); </script>';

  	} else if($seltype=="report11"){

		$bal = $conn -> real_escape_string($_POST['bal']);   
		$billname = $conn -> real_escape_string($_POST['billname']);  
		$billname = str_replace('=', '_', base64_encode($billname));
    	$daterange = $conn -> real_escape_string($_POST['daterange']);
		function before ($thiss, $inthat)
		{
		    return substr($inthat, 0, strpos($inthat, $thiss));
		}

		function after ($thiss, $inthat)
		{
		    if (!is_bool(strpos($inthat, $thiss)))
		        return substr($inthat, strpos($inthat,$thiss)+strlen($thiss));
		}
		 
		$fromdate = before('-', $daterange); 
		$fromdate = strtotime($fromdate); 
		$todate = after('-', $daterange); 
		$todate = strtotime($todate);

    	echo '
    	<div class="card-body table-responsive"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
					<th>System_Date</th>
					<th>LR_Date</th>
					<th>Company</th>
					<th>Branch</th>
					<th>Bilty_No</th>
					<th>Bill_Type</th>
					<th>LR_By</th>
					<th>Veh_PlacedBy</th>
					<th>LR_No</th> 
					<th>Broker</th>
					<th>Billing_Party</th>
					<th>Truck_No</th>
					<th>From</th>
					<th>To</th> 
					<th>Wheeler</th>
					<th>Max_Weight</th>
					<th>Actual_Weight</th>
					<th>Charge_Weight</th>
					<th>Rate</th>
					<th>Freight</th>
					<th>Received</th>
					<th>Balance</th>
					<th>POD_Upload</th>
					<th>POD_Branch</th>
					<th>POD_Date</th>
					<th>Bill_No</th>
					<th>Bill_Amount</th> 
					<th>Bill_Weight</th>
					<th>Bill_Gen_Date</th> 
					<th>Billing_Branch</th> 
					<th>Bill_Require</th> 
				</tr>
		      </thead> 
		 	</table>
		</div>
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sAjaxSource": "report_billname_data.php?p='.$billname.'&f='.$fromdate.'&t='.$todate.'&b='.$bal.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
		"dom": "lBfrtip",
		"ordering": true,
		"buttons": [
		"copy", "csv", "excel", "print"
		],
		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		// "targets":[4],
		// "orderable":false,
		},
		],
		"aoColumns": [
		{ mData: "0" },
		{ mData: "1" },
		{ mData: "2" },
		{ mData: "3" },
		{ mData: "4" },
		{ mData: "5" },
		{ mData: "6" },
		{ mData: "7" },
		{ mData: "8" },
		{ mData: "9" },
		{ mData: "10" },
		{ mData: "11" },
		{ mData: "12" },
		{ mData: "13" },
		{ mData: "14" },
		{ mData: "15" },
		{ mData: "16" },
		{ mData: "17" },
		{ mData: "18" },
		{ mData: "19" },
		{ mData: "20" },
		{ mData: "21" },
		{ mData: "22" },
		{ mData: "23" },
		{ mData: "24" },
		{ mData: "25" },
		{ mData: "26" },
		{ mData: "27" },
		{ mData: "28" },
		{ mData: "29" },
		{ mData: "30" }
		],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  

		}); 
     
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		} ); </script>';

  	} else if($seltype=="report12"){

		$bal = $conn -> real_escape_string($_POST['bal']);   
		$vehplacer = $conn -> real_escape_string($_POST['vehplacer']);  
		$vehplacer = str_replace('=', '_', base64_encode($vehplacer));
    	$daterange = $conn -> real_escape_string($_POST['daterange']);
		function before ($thiss, $inthat)
		{
		    return substr($inthat, 0, strpos($inthat, $thiss));
		}

		function after ($thiss, $inthat)
		{
		    if (!is_bool(strpos($inthat, $thiss)))
		        return substr($inthat, strpos($inthat,$thiss)+strlen($thiss));
		}
		 
		$fromdate = before('-', $daterange); 
		$fromdate = strtotime($fromdate); 
		$todate = after('-', $daterange); 
		$todate = strtotime($todate);

    	echo '
    	<div class="card-body table-responsive"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
					<th>System_Date</th>
					<th>LR_Date</th>
					<th>Company</th>
					<th>Branch</th>
					<th>Bilty_No</th>
					<th>Bill_Type</th>
					<th>LR_By</th>
					<th>Veh_PlacedBy</th>
					<th>LR_No</th> 
					<th>Broker</th>
					<th>Billing_Party</th>
					<th>Truck_No</th>
					<th>From</th>
					<th>To</th> 
					<th>Wheeler</th>
					<th>Max_Weight</th>
					<th>Actual_Weight</th>
					<th>Charge_Weight</th>
					<th>Rate</th>
					<th>Freight</th>
					<th>Received</th>
					<th>Balance</th>
					<th>POD_Upload</th>
					<th>POD_Branch</th>
					<th>POD_Date</th>
					<th>Bill_No</th>
					<th>Bill_Amount</th> 
					<th>Bill_Weight</th>
					<th>Bill_Gen_Date</th> 
					<th>Billing_Branch</th> 
					<th>Bill_Require</th> 
				</tr>
		      </thead> 
		 	</table>
		</div>
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sAjaxSource": "report_vehplacer_data.php?p='.$vehplacer.'&f='.$fromdate.'&t='.$todate.'&b='.$bal.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
		"dom": "lBfrtip",
		"ordering": true,
		"buttons": [
		"copy", "csv", "excel", "print"
		],
		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		// "targets":[4],
		// "orderable":false,
		},
		],
		"aoColumns": [
		{ mData: "0" },
		{ mData: "1" },
		{ mData: "2" },
		{ mData: "3" },
		{ mData: "4" },
		{ mData: "5" },
		{ mData: "6" },
		{ mData: "7" },
		{ mData: "8" },
		{ mData: "9" },
		{ mData: "10" },
		{ mData: "11" },
		{ mData: "12" },
		{ mData: "13" },
		{ mData: "14" },
		{ mData: "15" },
		{ mData: "16" },
		{ mData: "17" },
		{ mData: "18" },
		{ mData: "19" },
		{ mData: "20" },
		{ mData: "21" },
		{ mData: "22" },
		{ mData: "23" },
		{ mData: "24" },
		{ mData: "25" },
		{ mData: "26" },
		{ mData: "27" },
		{ mData: "28" },
		{ mData: "29" },
		{ mData: "30" }
		],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  

		}); 
     
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		} ); </script>';

  	} else if($seltype=="report13"){

		$bal = $conn -> real_escape_string($_POST['bal']);   
		$truck = $conn -> real_escape_string($_POST['truck']);   
    	$daterange = $conn -> real_escape_string($_POST['daterange']);
		function before ($thiss, $inthat)
		{
		    return substr($inthat, 0, strpos($inthat, $thiss));
		}

		function after ($thiss, $inthat)
		{
		    if (!is_bool(strpos($inthat, $thiss)))
		        return substr($inthat, strpos($inthat,$thiss)+strlen($thiss));
		}
		 
		$fromdate = before('-', $daterange); 
		$fromdate = strtotime($fromdate); 
		$todate = after('-', $daterange); 
		$todate = strtotime($todate);

    	echo '
    	<div class="card-body table-responsive"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
					<th>System_Date</th>
					<th>LR_Date</th>
					<th>Company</th>
					<th>Branch</th>
					<th>Bilty_No</th>
					<th>Bill_Type</th>
					<th>LR_By</th>
					<th>Veh_PlacedBy</th>
					<th>LR_No</th> 
					<th>Broker</th>
					<th>Billing_Party</th>
					<th>Truck_No</th>
					<th>From</th>
					<th>To</th> 
					<th>Wheeler</th>
					<th>Max_Weight</th>
					<th>Actual_Weight</th>
					<th>Charge_Weight</th>
					<th>Rate</th>
					<th>Freight</th>
					<th>Received</th>
					<th>Balance</th>
					<th>POD_Upload</th>
					<th>POD_Branch</th>
					<th>POD_Date</th>
					<th>Bill_No</th>
					<th>Bill_Amount</th> 
					<th>Bill_Weight</th>
					<th>Bill_Gen_Date</th> 
					<th>Billing_Branch</th> 
					<th>Bill_Require</th> 
				</tr>
		      </thead> 
		 	</table>
		</div>
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sAjaxSource": "report_truck_data.php?p='.$truck.'&f='.$fromdate.'&t='.$todate.'&b='.$bal.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
		"dom": "lBfrtip",
		"ordering": true,
		"buttons": [
		"copy", "csv", "excel", "print"
		],
		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		// "targets":[4],
		// "orderable":false,
		},
		],
		"aoColumns": [
		{ mData: "0" },
		{ mData: "1" },
		{ mData: "2" },
		{ mData: "3" },
		{ mData: "4" },
		{ mData: "5" },
		{ mData: "6" },
		{ mData: "7" },
		{ mData: "8" },
		{ mData: "9" },
		{ mData: "10" },
		{ mData: "11" },
		{ mData: "12" },
		{ mData: "13" },
		{ mData: "14" },
		{ mData: "15" },
		{ mData: "16" },
		{ mData: "17" },
		{ mData: "18" },
		{ mData: "19" },
		{ mData: "20" },
		{ mData: "21" },
		{ mData: "22" },
		{ mData: "23" },
		{ mData: "24" },
		{ mData: "25" },
		{ mData: "26" },
		{ mData: "27" },
		{ mData: "28" },
		{ mData: "29" },
		{ mData: "30" }
		],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  

		}); 
     
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		} ); </script>';
  	}