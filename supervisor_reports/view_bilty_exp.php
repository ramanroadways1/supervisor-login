<?php

require('../connect.php');
// error_reporting(0);

$bilty_no = escapeString($conn,strtoupper($_REQUEST['p']));

?>
<div class="card-body "  style="padding: 10px 25px; background-color: #fff; border: 1px solid #ccc;">
  <div class="row">
 
 <table class="table table-bordered" style="margin: 0px;">


  <tr style="text-align: center;">
    <th>Expense ID</th>
    <th>Expense Name</th>
    <th>Expense Code</th>
    <th>Amount</th>
    <th>Narration</th>
    <th>Branch</th> 
    <th>DateTime</th>
  </tr>
<?php
 
$qry = Qry($conn_rrpl,"SELECT trip_exp.* from diesel_api.all_trips RIGHT JOIN dairy.trip_exp ON dairy.trip_exp.trip_id = diesel_api.all_trips.id where diesel_api.all_trips.lr_type like '%$bilty_no%'");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

while($row=fetchArray($qry)){
 
?>
            <tr>
              <td> <?php echo $row['trans_id']; ?> </td> 
              <td> <?php echo $row['exp_name']; ?> </td> 
              <td> <?php echo $row['exp_code']; ?> </td> 
              <td> <?php echo $row['amount']; ?> </td> 
              <td> <?php echo $row['narration']; ?> </td> 
              <td> <?php echo $row['branch']; ?> </td>  
              <td> <?php echo $row['timestamp']; ?> </td> 
            </tr>
<?php
}
?>
            
            
          </table>
  </div>
</div>

<?php ?>
