<?php
require('../connect.php');

$bilty_no = escapeString($conn,strtoupper($_REQUEST['p']));

$qry = Qry($conn,"SELECT m.billing_branch, m.bill_require, m.bill_no, m.bill_amount, m.company, m.weight, m.bill_datetime,m.date,m.lrdate,m.bilty_no,
m.lr_by,m.billing_type,m.veh_placer,m.plr,m.broker,bill.name as billing_party,m.tno,m.frmstn,m.tostn,m.awt,m.wt,m.rate,m.tamt,m.branch,
bill.gst as bill_gst,bill.pan as bill_pan,broker.pan as broker_pan 
FROM mkt_bilty AS m 
LEFT OUTER JOIN dairy.billing_party AS bill ON bill.id = m.bill_party_id 
LEFT OUTER JOIN dairy.broker AS broker ON broker.id = m.broker_id 
WHERE m.bilty_no='$bilty_no'");
 
if(!$qry){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

$row = fetchArray($qry);

$b1 = $row['bill_no'];
$b2 = $row['bill_amount'];
$b3 = $row['weight'];
$b4 = $row['bill_datetime'];
?> 
  
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<style type="text/css">
	.applyBtn{
		border-radius: 0px !important;
	}
	.show-calendar{
		top: 180px !important;
	} 
    .applyBtn{
        border-radius: 0px !important;
    }
    table.table-bordered.dataTable td {
        padding: 10px 5px 10px 10px;
    }
     .dt-buttons{float: right;}
    .user_data_filter{
        float: right;
    }

    .dt-button {
        padding: 5px 20px;
        text-transform: uppercase;
        font-size: 12px;
        text-align: center;
        cursor: pointer;
        outline: none;
        color: #fff;
        background-color: #37474f ;
        border: none;
        border-radius:  2px;
        box-shadow: 0 4px #999;
    }

    .dt-button:hover {background-color: #3e8e41}

    .dt-button:active {
        background-color: #3e8e41;
        box-shadow: 0 5px #666;
        transform: translateY(4px);
    }
    #user_data_wrapper{
        width: 100% !important;
    }
    .dt-buttons{
        margin-bottom: 20px;
    }


#appenddiv, #appenddiv2 {
    display: block; 
    position:relative
} 
.ui-autocomplete {
    position: absolute;
}
 
.table-hover tbody tr:hover td,.table-hover tbody tr:hover th{background-color:#ffedda}.table td{vertical-align:middle!important;font-size:11px!important;color:#000;font-family:Verdana,Geneva,sans-serif;padding-top:4px;padding-right:4px;padding-bottom:4px;padding-left:10px}#user_data_info,#user_data_length{float:left}#user_data_filter,#user_data_paginate{float:right}.paginate_button{color:#000;float:left;padding:6px 12px;text-decoration:none;border:1px solid #ccc;cursor:pointer}.ellipsis{display:none}[type=search]{margin-right:10px; width: 250px; }.ui-autocomplete{z-index:2150000000!important}.container input{position:absolute;opacity:0;cursor:pointer;height:0;width:0}.checkmark{border-radius:2px;position:absolute;top:0;height:20px;width:20px;background-color:#fff;border:1px solid #000}.container:hover input~.checkmark{background-color:#fff}.container input:checked~.checkmark{background-color:#fff}.container input:disabled~.checkmark{background-color:#eaecf4}.checkmark:after{content:"";position:absolute;display:none}.container input:checked~.checkmark:after{display:block}.container .checkmark:after{left:6px;top:-1px;width:8px;height:16px;border:solid #000;border-width:0 3px 3px 0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}button:disabled,button[disabled]{border:1px solid #333!important;color:#333!important;cursor:no-drop} .table .thead-light th{text-align: center; font-size: 11px; color:#444;} .component{display: none;} 
	table {width: 100% !important;} table.table-bordered.dataTable td { white-space: nowrap; overflow: hidden; text-overflow:ellipsis;  }
  .table .thead-light th{
    text-transform: none !important;
  }
  .table th{
    max-width: 70px !important;
    font-size: 12px !important;
  }
  .table td{
    font-size: 12px !important; 
  }

</style> 
<div class="col-md-10 offset-md-1" style=""> <h4> Market Bilty: <?php echo $bilty_no; ?> </h4> </div>
   
<div class="col-md-10 offset-md-1" style="margin-bottom: 23px;">
<div class="card-body "  style="padding: 10px 25px; background-color: #fff; border: 1px solid #ccc;">
  <div class="row">
 
 <table class="table table-bordered" style="margin: 0px;">
            <tr>
              <th>Bilty No :</th> <td> <?php echo $bilty_no; ?> </td> 
              <th>Bilty Branch :</th> <td> <?php echo $row['branch']; ?> </td> 
            </tr>
            
            <tr>
              <th>Company :</th> <td> <?php echo $row['company']; ?> </td> 
              <th>Bilty Date :</th> <td> <?php echo $row['date']; ?> </td> 
            </tr>
            
            <tr>
              <th>LR Date :</th> <td> <?php echo $row['lrdate']; ?> </td> 
              <th>LR By :</th> <td> <?php echo $row['lr_by']; ?> </td> 
            </tr>
            
            <tr>
              <th>Billing Type :</th> <td> <?php echo $row['billing_type']; ?> </td> 
              <th>Veh. Placed By :</th> <td> <?php echo $row['veh_placer']; ?> </td> 
            </tr>
            
            <tr>
              <th>Party LR No :</th> <td> <?php echo $row['plr']; ?> </td> 
              <th>Truck No :</th> <td> <?php echo $row['tno']; ?> </td> 
            </tr>
  <?php

  $tsql = $conn->query("SELECT m.*,o.model 
  FROM dairy.own_truck AS o 
  LEFT OUTER JOIN max_weight_wheeler_wise AS m ON m.wheeler = o.wheeler 
  WHERE o.tno='$row[tno]'");
 
	if(!$tsql){
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
 
  $tres = $tsql->fetch_assoc();

  ?>        
            <tr>
              <th>Truck Wheeler :</th> <td> <?php echo $tres['wheeler']; ?> (Model No: <?php echo $tres['model']; ?> ) </td> 
              <th>Truck Max Weight :</th> <td> <?php echo $tres['max_weight']; ?> </td> 
            </tr>
            <tr>
              <th>Broker :</th> <td colspan="3"> <?php echo $row['broker']; ?> (PAN No: <?php echo $row['broker_pan']; ?>)</td> 
            </tr>
            
            <tr>
              <th>Billing Party :</th> <td colspan="3"> <?php echo $row['billing_party']; ?> (GST No: <?php echo $row['bill_gst']; ?>)</td> 
            </tr>
             

            <tr>
              <th>From Station :</th> <td> <?php echo $row['frmstn']; ?> </td> 
              <th>To Station :</th> <td> <?php echo $row['tostn']; ?> </td> 
            </tr>
            
            <tr>
              <th>Actual Weight :</th> <td> <?php echo $row['awt']; ?> </td> 
              <th>Charge Weight :</th> <td> <?php echo $row['cwt']; ?> </td> 
            </tr>
            
            <tr>
              <th>Rate :</th> <td> <?php echo $row['rate']; ?> </td> 
              <th>Freight :</th> <td> <?php echo $row['tamt']; ?> </td> 
            </tr>

            <tr>
              <th>Billing Branch :</th> <td> <?php echo $row['billing_branch']; ?> </td> 
              <th>Bill Require :</th> <td> <?php  if($row["bill_require"]=="1"){
            echo "YES"; 
            } else {
           echo  "NO"; 
            }
     ?> </td> 
            </tr>
            
          </table>
  </div>
</div>
</div>
 
 <div class="col-md-10 offset-md-1" style="margin-bottom: 23px;">
<div class="card-body "  style="padding: 10px 25px; background-color: #fff; border: 1px solid #ccc;">
  <div class="row">
 
 <table class="table table-bordered" style="margin: 0px;">


  <tr style="text-align: center;">
    <th>Advance ID</th>
    <th>Type</th>
    <th>Branch</th>
    <th>Dated</th>
    <th>Payment Mode</th>
    <th>UTR No / CHQ No</th>
    <th>Total Freight</th>
    <th>Received</th>
    <th>Balance</th>
    <th>Narration</th>
    <th>DateTime</th>
  </tr>
<?php
 
$qry = Qry($conn,"SELECT (SELECT narration from dairy.freight_adv where trans_id = dairy.bilty_book.trans_id) 
as trans, dairy.bilty_book.* 
FROM dairy.bilty_book 
WHERE bilty_no='$bilty_no'");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

while($row=fetchArray($qry)){
 
?>
            <tr>
              <td> <?php echo $row['trans_id']; ?> </td> 
              <td> <?php echo $row['type']; ?> </td> 
              <td> <?php echo $row['branch']; ?> </td> 
              <td> <?php echo $row['date']; ?> </td> 
              <td> <?php echo $row['trans_type']; ?> </td> 
              <td> <?php echo $row['trans_value']; ?> </td> 
              <td> <?php echo $row['credit']; ?> </td> 
              <td> <?php echo $row['debit']; ?> </td> 
              <td> <?php echo $row['balance']; ?> </td> 
              <td> <?php  if($row['trans_id']!=""){
                          $nar = $row['trans'];
                          } else {
                          $nar = $row["narration"];
                          } 
                          echo $nar; ?> </td> 
              <td> <?php echo $row['timestamp']; ?> </td> 
            </tr>
<?php
}
?>
            
            
          </table>
  </div>
</div>
</div>


<div class="col-md-10 offset-md-1" style="margin-bottom: 23px;" id="get_content2">
<p id="pro" style="display: none;"> <font color="orange">Loading Expense...</font> </p>
</div>

<script type="text/javascript">

  $(document).ready(function(){

    $('#pro').show(); 
           $.ajax({  
                url:"view_bilty_exp.php?p=<?= $bilty_no; ?>",  
                method:"post",   
                success:function(data){  
                     $('#get_content2').html(data);   
                     $('#pro').hide(); 
                }  
           }); 
  });
</script>


<?php
 
$qry = Qry($conn,"SELECT dairy.rcv_pod.*, dairy.bilty_narration.narration 
FROM dairy.rcv_pod 
LEFT outer JOIN dairy.bilty_narration on bilty_narration.bilty_no = rcv_pod.lrno where lrno='$bilty_no'");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

$row = fetchArray($qry);

$pod_files1 = array(); 
$copy_no = 0;

foreach(explode(",",$row['copy']) as $pod_copies){
	$copy_no++;
	$pod_files1[] = "<a href='https://rrpl.online/diary/close_trip/$pod_copies' target='_blank'>Upload: $copy_no</a>";
}

$podcopy = implode(", ",$pod_files1);
?>

<div class="col-md-10 offset-md-1" style="margin-bottom: 23px;">
<div class="card-body "  style="padding: 10px 25px; background-color: #fff; border: 1px solid #ccc;">
  <div class="row">
 
 <table class="table table-bordered" style="margin: 0px;">
            <tr style="text-align: center;">
              <th>POD DateTime  </th>
              <th>POD Branch  </th>
             <th>POD Narration </th> 
              <th>POD Uploads </th>
            </tr>
            
            <tr>
               <td><?php echo $row['timestamp']; ?> </td> 
                <td>  <?php echo $row['branch']; ?> </td>
             <td> <?php echo $row['narration']; ?> </td>  
              <td> <?php if($row['copy']!=''){ echo $podcopy; } ?> </td> 
            </tr>
             
          </table>
  </div>
</div>
</div>


<?php
 
$qry = Qry($conn,"SELECT * FROM `bill_gst` where bill_no='$b1' order by id desc limit 1");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

$row = fetchArray($qry); 
?>

<div class="col-md-10 offset-md-1" style="margin-bottom: 23px;">
<div class="card-body "  style="padding: 10px 25px; background-color: #fff; border: 1px solid #ccc;">
  <div class="row">
 
 <table class="table table-bordered" style="margin: 0px;">
            <tr style="text-align: center;">
              <th> Bill No  </th>
              <th> Weight  </th>
              <th> SGST  </th>
              <th> CGST  </th>
              <th> IGST  </th>
              <th> Bill Amount </th> 
              <th> DateTime </th> 
            </tr>
            
            <tr>
               <td><?php echo $b1; ?> </td>  
               <td><?php echo $b3; ?> </td>  
               <td><?php echo $row['sgst_amt']; ?> </td>  
               <td><?php echo $row['cgst_amt']; ?> </td>  
               <td><?php echo $row['igst_amt']; ?> </td>   
               <td><?php echo $b2; ?> </td>  
               <td><?php echo $b4; ?> </td>  
            </tr>
             
          </table>
  </div>
</div>
</div>
