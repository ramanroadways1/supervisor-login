<?php

   require('connect.php');
   
   $b = $conn->real_escape_string($_REQUEST['b']);  
   $p = $conn->real_escape_string($_REQUEST['p']); 
   $p = str_replace('_', '=', base64_decode($p)); 
   $f = $conn->real_escape_string($_REQUEST['f']);
   $t = $conn->real_escape_string($_REQUEST['t']);

$f = date("Y-m-d",$f);
$t = date("Y-m-d",$t);

$connection = new PDO('mysql:host='.$host.';dbname='.$db_name.';', $username, $password );

if($b=="1"){ 
$statement = $connection->prepare("SELECT   m.billing_branch, m.bill_require, m.lr_by, m.company, m.bill_no, m.bill_amount, o.wheeler, m.weight, w.max_weight, m.bill_datetime as bill_date, m.date as sys_date, m.lrdate as lr_date, m.bilty_no, m.lr_by, m.billing_type, m.veh_placer, m.plr, m.broker, m.billing_party, m.tno, m.frmstn, m.tostn, m.awt, m.cwt, m.rate, m.tamt, m.branch, b.freight, b.rcvd, b.balance, b.pod, b.pod_copy, n.amount, r.branch as podbranch, n.narration,  r.date as poddate, n.recovery_date, n.cleared FROM mkt_bilty m
  LEFT JOIN dairy.bilty_balance b on b.bilty_no = m.bilty_no
  LEFT JOIN dairy.bilty_narration n on n.bilty_no = m.bilty_no
  LEFT JOIN dairy.rcv_pod r on r.lrno = m.bilty_no
  LEFT JOIN dairy.own_truck o on o.tno = m.tno
  LEFT JOIN max_weight_wheeler_wise w on w.wheeler = o.wheeler
  WHERE m.date BETWEEN '$f' AND '$t' AND m.billing_party='$p'");
} else { 
$statement = $connection->prepare("SELECT   m.billing_branch, m.bill_require, m.lr_by, m.company, m.bill_no, m.bill_amount, o.wheeler, m.weight, w.max_weight, m.bill_datetime as bill_date, m.date as sys_date, m.lrdate as lr_date, m.bilty_no, m.lr_by, m.billing_type, m.veh_placer, m.plr, m.broker, m.billing_party, m.tno, m.frmstn, m.tostn, m.awt, m.cwt, m.rate, m.tamt, m.branch, b.freight, b.rcvd, b.balance, b.pod, b.pod_copy, n.amount, r.branch as podbranch, n.narration,  r.date as poddate, n.recovery_date, n.cleared FROM mkt_bilty m
  LEFT JOIN dairy.bilty_balance b on b.bilty_no = m.bilty_no
  LEFT JOIN dairy.bilty_narration n on n.bilty_no = m.bilty_no
  LEFT JOIN dairy.rcv_pod r on r.lrno = m.bilty_no
  LEFT JOIN dairy.own_truck o on o.tno = m.tno
  LEFT JOIN max_weight_wheeler_wise w on w.wheeler = o.wheeler
  WHERE m.date BETWEEN '$f' AND '$t' AND m.billing_party='$p' AND b.balance!='0'");
}

$statement->execute();
$result = $statement->fetchAll();
$count = $statement->rowCount();
$data = array();

$sno=0;
foreach($result as $row)
{ 
  $sno = $sno+1;
	$sub_array = array();  
            $sub_array[] = $row["sys_date"];
            $sub_array[] = $row["lr_date"];
            $sub_array[] = $row["company"];
            $sub_array[] = $row["branch"];
            $sub_array[] = "<a href='view_bilty.php?p=".$row["bilty_no"]."' style='color: #000;' target='_blank'>".$row["bilty_no"]."</a>";
            $sub_array[] = $row["billing_type"];
            $sub_array[] = $row["lr_by"];
            $sub_array[] = $row["veh_placer"];
            $sub_array[] = $row["plr"];
            $sub_array[] = $row["broker"]; 
            $sub_array[] = $row["billing_party"]; 
            $sub_array[] = $row["tno"];
            $sub_array[] = $row["frmstn"];
            $sub_array[] = $row["tostn"]; 
            $sub_array[] = $row["wheeler"];
            $sub_array[] = $row["max_weight"];
            $sub_array[] = $row["awt"];
            $sub_array[] = $row["cwt"];
            $sub_array[] = $row["rate"];
            $sub_array[] = $row["tamt"];
            $sub_array[] = $row["rcvd"];
            $sub_array[] = $row["balance"];
$pod_files1 = array(); 
$copy_no = 0;
foreach(explode(",",$row['pod_copy']) as $pod_copies)
{
$copy_no++;
$pod_files1[] = "<a href='https://rrpl.online/diary/close_trip/$pod_copies' target='_blank'>Upload: $copy_no</a>";
}
  if($row['pod']=="1"){
    $pod_file = implode(", ",$pod_files1); 
  } else {
    $pod_file = "Not Received";
  }
            $sub_array[] = $pod_file; 
            $sub_array[] = $row["podbranch"];
            $sub_array[] = $row["poddate"]; 
            $sub_array[] = $row["bill_no"];
            $sub_array[] = $row["bill_amount"]; 
            $sub_array[] = $row["weight"];
            $sub_array[] = $row["bill_date"];
            $sub_array[] = $row["billing_branch"];
            if($row["bill_require"]=="1"){
            $sub_array[] = "YES"; 
            } else {
            $sub_array[] = "NO"; 
            }
	$data[] = $sub_array;

} 

$results = array(
	"sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>
 