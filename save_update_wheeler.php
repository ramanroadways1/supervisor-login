<?php
require_once("./_connect.php");
 
$timestamp = date("Y-m-d H:i:s");
$date = date("Y-m-d");

$tno = escapeString($conn,($_POST['tno']));
$wheeler = escapeString($conn,($_POST['wheeler']));

$update_code = Qry($conn,"UPDATE dairy.own_truck SET wheeler='$wheeler' WHERE tno='$tno'");

if(!$update_code){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

// AlertRightCornerSuccess("Approved Successfully !");
 
echo "<script>
	Swal.fire({icon: 'success',html: '<font size=\'2\' color=\'black\'>Updated Success !</font>',});
	$('#loadicon').fadeOut('slow');
	$('#add_btn').attr('disabled',true);
	$('#add_btn').hide();
</script>";
exit();
?>