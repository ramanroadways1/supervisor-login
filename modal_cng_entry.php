<?php
require_once("_connect.php");

$date = date("Y-m-d");

$id = escapeString($conn,($_POST['id'])); 

if(empty($id)){
	AlertRightCornerError("Trip Id not found !");
	exit();
}

$qry = Qry($conn,"SELECT tno,branch,from_station,to_station,cash FROM dairy.trip WHERE id='$id'");
	
if(numRows($qry)==0)
{
	AlertRightCornerError("No record found !");
	exit();
}

$_SESSION['advance_cash_cng'] = $id;

$row = fetchArray($qry);
?>

<script type="text/javascript">
function sum1() 
{
	if($("#qty").val()==''){
		$("#qty").val('0');
	}
						
	if($("#rate").val()==''){
		$("#rate").val('0');
	}
							
	$("#dsl_amt").val(Math.round(Number($("#qty").val()) * Number($("#rate").val())).toFixed(2));
}
					
function sum2() 
{
	if($("#qty").val()==''){
		$("#qty").val('0');
	}
						
	if($("#rate").val()==''){
		$("#rate").val('0');
	}
							
	$("#dsl_amt").val(Math.round(Number($("#qty").val()) * Number($("#rate").val())).toFixed(2));
}
					 
function sum3() 
{
	if($("#qty").val()==''){
		$("#qty").val('0');
	}
						
	if($("#rate").val()==''){
		$("#rate").val('0');
	}
						
	if($("#dsl_amt").val()==''){
		$("#dsl_amt").val('0');
	}

	$("#rate").val((Number($("#dsl_amt").val()) / Number($("#qty").val())).toFixed(2));
}
</script>
				

		<div class="row">
			
			<div class="form-group col-md-6">
				<label>Qty <font color="red"><sup>*</sup></font></label>
				<input type="number" step="any" class="form-control" oninput="sum2();" id="qty" name="qty" required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>Rate <font color="red"><sup>*</sup></font></label>
				<input type="number" step="any" class="form-control" oninput="sum1();" id="rate" name="rate" required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>Amount <font color="red"><sup>*</sup></font></label>
				<input type="number" oninput="sum3();" class="form-control" id="dsl_amt" name="amount" required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>Branch <font color="red"><sup>*</sup></font></label>
				<select name="branch" class="form-control" required="required">
					<option value="">--select branch--</option>
					<?php
					$get_branch = Qry($conn,"SELECT username FROM user WHERE role='2' AND username NOT in('DUMMY','HEAD')");
					
					while($row_branch = fetchArray($get_branch))
					{
					?>
					<option <?php if($row_branch['username']==$row['branch']) { echo "selected"; } ?> value="<?php echo $row_branch['username']; ?>"><?php echo $row_branch['username']; ?></option>
					<?php					
					}
					?>
				</select>
			</div>
			
			<input type="hidden" name="trip_id" value="<?php echo $id; ?>">
			
			<div class="form-group col-md-6">
				<label>Date <font color="red"><sup>*</sup></font></label>
				<input type="date" name="date" max="<?php echo date("Y-m-d"); ?>" class="form-control" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" required />
			</div>
			
			<div class="form-group col-md-6">
				<label>Narration <font color="red"><sup>*</sup></font></label>
				<textarea class="form-control" name="narration" oninput="this.value=this.value.replace(/[^A-Z a-z0-9,#.@/:;-]/,'');" required="required"></textarea>
			</div>
		</div> 
	

<script>
$('#modal_cng_btn')[0].click();	
$('#loadicon').fadeOut('slow');	
</script> 


