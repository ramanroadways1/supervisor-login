<?php
require_once("_connect.php");

$date = date("Y-m-d");

$report_type = escapeString($conn,($_POST['report_type']));  // trips_today ,market_bilty ,hisab_today ,free_vehicle

if(empty($report_type)){
	AlertRightCornerError("Report type not found !");
	exit();
}

if($report_type=='trips_today')
{
	$report_name = "Trips Today";
	
	$get_report = Qry($conn,"SELECT tno,trip_no,branch,from_station,to_station,lr_type,lrno,act_wt,charge_wt FROM dairy.trip WHERE date(date)='$date' 
	AND tno in(SELECT tno from dairy.own_truck WHERE superv_id='$_SESSION[supervisor_id]')");
}
else if($report_type=='market_bilty')
{
	$report_name = "Market Bilty Today";
	
	$get_report = Qry($conn,"SELECT b.lrdate,b.bilty_no,b.tno,b.frmstn,b.tostn,b.awt,b.cwt,b.rate,b.tamt,b.branch,u.name as username 
	FROM mkt_bilty AS b 
	LEFT OUTER JOIN emp_attendance AS u ON u.code = b.branch_user 
	WHERE b.date='$date' AND b.tno in(SELECT tno from dairy.own_truck WHERE superv_id='$_SESSION[supervisor_id]')");
}
else if($report_type=='hisab_today')
{
	$report_name = "Hisab Today";
	
	$get_report = Qry($conn,"SELECT t.tno,t.trip_no,t.loaded_final,t.hisab_type,t.branch,d.name as driver_name,u.name as hisab_user 
	FROM dairy.log_hisab AS t 
	LEFT OUTER JOIN emp_attendance AS u ON u.code = t.branch_user 
	LEFT OUTER JOIN dairy.driver AS d ON d.code = t.driver 
	WHERE date(t.timestamp)='$date' AND tno in(SELECT tno from dairy.own_truck WHERE superv_id='$_SESSION[supervisor_id]')");
}
else if($report_type=='free_vehicle')
{
	$report_name = "Free Vehicles";
	
	$get_report = Qry($conn,"SELECT tno,comp as company FROM dairy.own_truck WHERE tno in(SELECT tno from dairy.own_truck WHERE is_sold!='1' AND 
	superv_id='$_SESSION[supervisor_id]') AND tno NOT IN(SELECT tno FROM dairy.trip)");
}
else
{
	AlertRightCornerError("Invalid Report type !");
	exit();
}

if(!$get_report){
	AlertRightCornerError("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($get_report)==0)
{
	AlertRightCornerError("No record found !");
	exit();
}
?>
<button id="modal_btn1" style="display:none" data-toggle="modal" data-target="#ModalReport"></button>

<div class="modal fade" id="ModalReport" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
		<div class="modal-content" style="">
			<div class="modal-header bg-primary">
				<span style="font-size:13px">Report : <?php echo $report_name; ?></span>
			</div>
	<div class="modal-body">
		<div class="row">
			<div style="height:400px !important;overflow:auto" class="col-md-12 table-responsive">
<?php
if($report_type=='trips_today')
{
	?>
				<table id="example" class="table table-bordered table-striped">
					<tr>
                        <th>#</th>
                        <th>Vehicle_No</th>
                        <th>Trip_No</th>
                        <th>Branch</th>
                        <th>Trip</th>
                        <th>LR_Type</th>
                        <th>LR_No</th>
                        <th>Act_Wt</th>
                        <th>Chrg_Wt</th>
                    </tr>
				<?php
				$sn=1;				
				while($row = fetchArray($get_report))
				{
					echo "<tr>
						<td>$sn</td>
						<td>$row[tno]</td>
						<td>$row[trip_no]</td>
						<td>$row[branch]</td>
						<td>$row[from_station] to $row[to_station]</td>
						<td>$row[lr_type]</td>
						<td>$row[lrno]</td>
						<td>$row[act_wt]</td>
						<td>$row[charge_wt]</td>
					</tr>";
				$sn++;	
				}
				?>				
				</table>	
	
	<?php
}
else if($report_type=='market_bilty')
{
	?>
			<table id="example" class="table table-bordered table-striped">
					<tr>
                        <th>#</th>
                        <th>Vehicle_No</th>
                        <th>LR_Date</th>
                        <th>Bilty_No</th>
						<th>Trip</th>
                        <th>Act_Wt</th>
                        <th>Chrg_Wt</th>
                        <th>Rate</th>
                        <th>Freight</th>
                        <th>Branch & User</th>
                    </tr>
				<?php
				$sn=1;				
				while($row = fetchArray($get_report))
				{
					$lr_date = date("d-m-y",strtotime($row['lrdate']));
					
					echo "<tr>
						<td>$sn</td>
						<td>$row[tno]</td>
						<td>$lr_date</td>
						<td>$row[bilty_no]</td>
						<td>$row[frmstn] to $row[tostn]</td>
						<td>$row[awt]</td>
						<td>$row[cwt]</td>
						<td>$row[rate]</td>
						<td>$row[tamt]</td>
						<td>$row[branch]<br>($row[username])</td>
					</tr>";
				$sn++;	
				}
				?>				
				</table>	
	<?php
}
else if($report_type=='hisab_today')
{
	?>
				<table id="example" class="table table-bordered table-striped">
					<tr>
                        <th>#</th>
                        <th>Vehicle_No</th>
                        <th>Driver</th>
                        <th>Trip_No</th>
                        <th>Loaded/Final</th>
                        <th>Hisab_Type</th>
                        <th>Branch & User</th>
                    </tr>
				<?php
				$sn=1;				
				while($row = fetchArray($get_report))
				{
					// $lr_date = date("d-m-y",strtotime($row['lrdate']));
				
				if($row['loaded_final']=="0"){
					$loaded_final = "Loaded";
				}
				else{
					$loaded_final = "Final";
				}
				
				if($row['hisab_type']=="1"){
					$hisab_type = "Carry-Fwd";
				}
				else if($row['hisab_type']=="2"){
					$hisab_type = "Pay";
				}
				else if($row['hisab_type']=="3"){
					$hisab_type = "Carry-Fwd+Pay";
				}
				else if($row['hisab_type']=="4"){
					$hisab_type = "Credit-HO";
				}
				
					echo "<tr>
						<td>$sn</td>
						<td>$row[tno]</td>
						<td>$row[driver_name]</td>
						<td>$row[trip_no]</td>
						<td>$loaded_final</td>
						<td>$hisab_type</td>
						<td>$row[branch]<br>($row[hisab_user])</td>
					</tr>";
				$sn++;	
				}
				?>				
				</table>
	<?php
}
else if($report_type=='free_vehicle')
{
	?>
				<table id="example" class="table table-bordered table-striped">
					<tr>
                        <th>#</th>
                        <th>Vehicle_No</th>
                        <th>Company</th>
                    </tr>
				<?php
				$sn=1;				
				while($row = fetchArray($get_report))
				{
					echo "<tr>
						<td>$sn</td>
						<td>$row[tno]</td>
						<td>$row[company]</td>
					</tr>";
				$sn++;	
				}
				?>				
				</table>
	<?php
}
?>			
			</div>
		</div>
	</div>
	
		<div class="modal-footer">
			<button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
		</div>
	 
      </div>
    </div>
  </div>

<script>
$('#modal_btn1')[0].click();	
$('#loadicon').fadeOut('slow');	
</script> 
