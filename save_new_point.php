<?php
require_once("./_connect.php");
 
$timestamp = date("Y-m-d H:i:s");
$date = date("Y-m-d");

$location = escapeString($conn,($_POST['location']));
$loc_lat_long = escapeString($conn,($_POST['loc_lat_long']));
$party_id = escapeString($conn,($_POST['party_id']));
$way_to_add = escapeString($conn,($_POST['way_to_add']));
$own_tno = escapeString($conn,($_POST['own_tno']));
$visit_date = escapeString($conn,($_POST['visit_date']));
$google_lat = escapeString($conn,($_POST['google_lat']));
$loading_point = escapeString($conn,($_POST['loading_point']));
$google_lng = escapeString($conn,($_POST['google_lng']));
$label = escapeString($conn,($_POST['label']));
$google_pincode = escapeString($conn,($_POST['loading_pincode']));
$branch = escapeString($conn,($_POST['branch']));
$google_distance = escapeString($conn,($_POST['google_distance']));
$poi_type = escapeString($conn,($_POST['poi_type']));
$cord_lat = trim(escapeString($conn,($_POST['cord_lat'])));
$cord_long = trim(escapeString($conn,($_POST['cord_long'])));

if($poi_type=='loading')
{
	$table_name = "address_book_consignor";
	$location_col = "from_id";
	$party_col = "consignor";
}
else
{
	$table_name = "address_book_consignee";
	$location_col = "to_id";
	$party_col = "consignee";
}

if($location=='' || $party_id=='')
{
	AlertErrorTopRight("Location or Party not found !");
	echo "<script>
		$('#spinner_icon').hide();
		$('#submit_icon').show();
	</script>";
	exit();
}

if($way_to_add=='Own_Truck')
{
	$lat = explode(",",$loading_point)[0];
	$long = explode(",",$loading_point)[1];
	$poi_from = "GPS";
	
	if($lat=='' || $long=='')
	{
		AlertErrorTopRight("Coordinates not found !");
		echo "<script>
			$('#spinner_icon').hide();
			$('#submit_icon').show();
		</script>";
		exit();
	}
	
	$pincode = escapeString($conn,($_POST['pincode_own_tno']));
	
	if(strlen($pincode)!=6)
	{
		AlertErrorTopRight("Pincode not found !");
		echo "<script>
			$('#spinner_icon').hide();
			$('#submit_icon').show();
		</script>";
		exit();
	}
}
else if($way_to_add=='Coordinates')
{
	$poi_from = "Coordinates";
	
	$lat = $cord_lat;
	$long = $cord_long;
	
	if($lat=='' || $long=='')
	{
		AlertErrorTopRight("Coordinates not found !");
		echo "<script>
			$('#spinner_icon').hide();
			$('#submit_icon').show();
		</script>";
		exit();
	}
	
	$origin = $loc_lat_long;
	$destination = $lat.",".$long;
	
	$url="https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".urlencode($origin)."&destinations=".urlencode($destination)."&key=AIzaSyCZ6JUFGB8r5TpRYGnJjNxUH1NZdxaPoGw";
	$api = file_get_contents($url);
	$data = json_decode($api);
				
	$api_status = $data->rows[0]->elements[0]->status;
		
	if($api_status=='NOT_FOUND')
	{
		AlertErrorTopRight("Distance not found !");
		echo "<script>
			$('#spinner_icon').hide();
			$('#submit_icon').show();
		</script>";
		exit();
	}

	if($api_status!='OK')
	{
		AlertErrorTopRight("API Error !");
		echo "<script>
			$('#spinner_icon').hide();
			$('#submit_icon').show();
		</script>";
		echo "<span style='color:red;font-size:13px'>API Error: $api_status !</span>";
		exit();
	}
			
	$dest_addr = $data->destination_addresses[0];
	$origin_addr = $data->origin_addresses[0];
	$distance = round((int)$data->rows[0]->elements[0]->distance->value / 1000);
	$travel_time = $data->rows[0]->elements[0]->duration->text;
	$travel_time_value = $data->rows[0]->elements[0]->duration->value;
	$travel_hrs = gmdate("H", $travel_time_value);
	$travel_minutes = gmdate("i", $travel_time_value);
	$travel_seconds = gmdate("s", $travel_time_value);
	
	$google_distance = $distance; 
	// if($distance>60)
	// {
		// AlertErrorTopRight("Distance between location and loading point is: $distance KMs !");
		// echo "<script>$('#loading_point').val('');</script>";
		// exit();
	// }

	$get_pincode = getZipcode($lat.",".$long);

	if(strlen($get_pincode)!=6)
	{
		AlertErrorTopRight("Unable to fetch pincode !");
		echo "<script>
			$('#spinner_icon').hide();
			$('#submit_icon').show();
		</script>";
		exit();
	}
	
	$pincode = $get_pincode;
}
else if($way_to_add=='Coordinates')
{
	$poi_from = "GOOGLE";
	$own_tno = "";
	$visit_date = "";
	
	if($google_lat=='' || $google_lng=='')
	{
		AlertErrorTopRight("Google location not found !");
		echo "<script>
			$('#spinner_icon').hide();
			$('#submit_icon').show();
		</script>";
		exit();
	}
	
	$lat = $google_lat;
	$long = $google_lng;
	$pincode = $google_pincode;

	$origin = $loc_lat_long;
	$destination = $lat.",".$long;
	
	$url="https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".urlencode($origin)."&destinations=".urlencode($destination)."&key=AIzaSyCZ6JUFGB8r5TpRYGnJjNxUH1NZdxaPoGw";
	$api = file_get_contents($url);
	$data = json_decode($api);
				
	$api_status = $data->rows[0]->elements[0]->status;
		
	if($api_status=='NOT_FOUND')
	{
		AlertErrorTopRight("Distance not found !");
		echo "<script>
			$('#spinner_icon').hide();
			$('#submit_icon').show();
		</script>";
		exit();
	}

	if($api_status!='OK')
	{
		AlertErrorTopRight("API Error !");
		echo "<script>
			$('#spinner_icon').hide();
			$('#submit_icon').show();
		</script>";
		echo "<span style='color:red;font-size:13px'>API Error: $api_status !</span>";
		exit();
	}
			
	$dest_addr = $data->destination_addresses[0];
	$origin_addr = $data->origin_addresses[0];
	$distance = round((int)$data->rows[0]->elements[0]->distance->value / 1000);
	$travel_time = $data->rows[0]->elements[0]->duration->text;
	$travel_time_value = $data->rows[0]->elements[0]->duration->value;
	$travel_hrs = gmdate("H", $travel_time_value);
	$travel_minutes = gmdate("i", $travel_time_value);
	$travel_seconds = gmdate("s", $travel_time_value);
	
	$google_distance = $distance; 
	// if($distance>60)
	// {
		// AlertErrorTopRight("Distance between location and loading point is: $distance KMs !");
		// echo "<script>$('#loading_point').val('');</script>";
		// exit();
	// }

	if(strlen($pincode)!=6)
	{
		$get_pincode = getZipcode($lat.",".$long);

		if(strlen($get_pincode)!=6)
		{
			AlertErrorTopRight("Unable to fetch pincode !");
			echo "<script>
				$('#spinner_icon').hide();
				$('#submit_icon').show();
			</script>";
			exit();
		}
		else
		{
			$pincode = $get_pincode;
		}
	}
	
	if(strlen($pincode)!=6)
	{
		AlertErrorTopRight("Google Pincode not found !");
		echo "<script>
			$('#spinner_icon').hide();
			$('#submit_icon').show();
		</script>";
		exit();
	}
}
else
{
	AlertErrorTopRight("Invalid option selected for way to add !");
		echo "<script>
			$('#spinner_icon').hide();
			$('#submit_icon').show();
		</script>";
		exit();
}

$check_poi = Qry($conn,"SELECT id FROM `$table_name` WHERE `$location_col`='$location' AND `$party_col`='$party_id'");

if(!$check_poi){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		$('#spinner_icon').hide();
		$('#submit_icon').show();
	</script>";
	exit();
}

if(numRows($check_poi)>0)
{
	AlertErrorTopRight("Duplicate record found for the party and location !");
	echo "<script>
		$('#spinner_icon').hide();
		$('#submit_icon').show();
	</script>";
	exit();
}

$insert = Qry($conn,"INSERT INTO `$table_name`(label,`$party_col`,`$location_col`,pincode,_lat,_long,google_km,branch,branch_user,record_by,
tno_visited,visit_date,timestamp) VALUES ('$label','$party_id','$location','$pincode','$lat','$long','$google_distance','$branch','GPS_ADMIN',
'$poi_from','$own_tno','$visit_date','$timestamp')");

if(!$insert){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		$('#spinner_icon').hide();
		$('#submit_icon').show();
	</script>";
	exit();
}

$this_id = getInsertID($conn);

$update_code = Qry($conn,"UPDATE `$table_name` SET code='$this_id' WHERE id='$this_id'");

if(!$update_code){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		$('#spinner_icon').hide();
		$('#submit_icon').show();
	</script>";
	exit();
}

$poi_type = strtoupper($poi_type);

 AlertRightCornerSuccess("$poi_type Point Added Successfully !");
 
	echo "<script>
		$('#Form1')[0].reset();
		$('#way_to_add').val('');
		$('.own_truck_div').show();
		$('.google_div').hide();
	</script>";
	
	echo "<script>
		$('#spinner_icon').hide();
		$('#submit_icon').show();
	</script>";
	exit();
?>