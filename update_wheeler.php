<?php
include("_header.php");
?>
<script>
$(function() {
		$("#own_tno").autocomplete({
		source: 'autofill/get_own_vehicle.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#own_tno').val(ui.item.value);   
            $('#wheeler').val(ui.item.wheeler);   
			
			if(ui.item.wheeler=='' || ui.item.wheeler==0)
			{
				$('#add_btn').attr('disabled',false);
				$('#add_btn').show();
			}
			else
			{
				$('#add_btn').attr('disabled',true);
				$('#add_btn').hide();
			}
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#own_tno').val("");   
			$('#wheeler').val("");   
			$('#add_btn').attr('disabled',true);
			$('#add_btn').hide();
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Vehicle does not exists.</font>',});
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
</script>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Update Wheeler : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				<div class="col-md-12">
					<div class="row">		

						
						<div class="form-group col-md-3">
							<label>Vehicle Number <sup><font color="red">*</font></sup></label>
							<input id="own_tno" name="own_tno" autocomplete="off" required="required" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'')" 
							type="text" class="form-control" />
						</div>
						
						<div class="form-group col-md-3">
							<label>Select Model <sup><font color="red">*</font></sup></label>
							<select name="wheeler" id="wheeler" class="form-control" required="required">
								<option value="">--select wheeler--</option>
								<?php
								$get_wheeler = Qry($conn,"SELECT wheeler FROM max_weight_wheeler_wise ORDER BY wheeler ASC");
								
								while($row_wheeler = fetchArray($get_wheeler))
								{
									echo "<option value='$row_wheeler[wheeler]'>$row_wheeler[wheeler]</option>";
								}
								?>
							</select>
						</div>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" onclick="UpdateWheeler()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn">
							<i id="spinner_icon" class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp; Update</button>
						</div>
						
				</div> 
				</div> 
                </div><!-- /.box-body --> 
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("_footer.php") ?>

<div id="func_result"></div>  

<script>
function UpdateWheeler()
{
	var wheeler = $('#wheeler').val();
	var tno = $('#own_tno').val();
	
	if(wheeler=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Select wheeler first !</font>',});
	}
	else if(tno=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter vehicle number first !</font>',});
	}
	else
	{
		$('#loadicon').show();
		$('#add_btn').attr('disabled',true);
		jQuery.ajax({
				url: "./save_update_wheeler.php",
				data: 'wheeler=' + wheeler + '&tno=' + tno,
				type: "POST",
				success: function(data) {
					$("#func_result").html(data);
				},
				error: function() {}
		});
	}
}
</script>
