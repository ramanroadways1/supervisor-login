<?php
require_once("_connect.php");

$date = date("Y-m-d");

$id = escapeString($conn,($_POST['id'])); 

if(empty($id)){
	AlertRightCornerError("Trip Id not found !");
	exit();
}

$qry = Qry($conn,"SELECT t.tno,t.branch,t.from_station,t.to_station,t.cash,t.fix_lane,o.trishul_card 
FROM dairy.trip AS t 
LEFT OUTER JOIN dairy.own_truck AS o ON o.tno = t.tno 
WHERE t.id='$id'");
	
if(numRows($qry)==0)
{
	AlertRightCornerError("No record found !");
	exit();
}

$_SESSION['exp_trip_id'] = $id;

$row = fetchArray($qry);

$trishul_card = $row['trishul_card'];
?>
	<div class="row">
			
			<?php 
			if($trishul_card=="1")
			{
			?>	
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					Trishul - Card active on this vehicle !
				  <button type="button" onclick="$('#exp_submit').attr('disabled',false);$('#exp_submit').html('Submit');" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				  </button>
				</div>
			</div>
			
			<script>
			$('#exp_submit').attr('disabled',true);
			$('#exp_submit').html('Trishul-Card active !');
			</script>
			<?php
			}
			?>
			
			<div class="form-group col-md-6">
				<label>Branch <font color="red"><sup>*</sup></font></label>
				<input type="text" class="form-control" value="<?php echo $row["branch"]; ?>" readonly required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>Date <font color="red"><sup>*</sup></font></label>
				<input type="text" class="form-control" value="<?php echo date("d-m-Y"); ?>" readonly required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>Amount <font color="red"><sup>*</sup></font></label>
				<input type="number" class="form-control" min="10" name="amount" required="required">
			</div> 
			
			<div class="form-group col-md-6">
				<label>Select Expense <font color="red"><sup>*</sup></font></label>
				<select name="exp_id" class="form-control" required="required">
					<option value="">--select expense--</option>
					<?php
					if($row['fix_lane']!=0)
					{
						$get_exp = Qry($conn,"SELECT id,name,exp_code,cap_amount FROM dairy.exp_head WHERE fix_entry_to_supervisor='1' ORDER BY name ASC");
					}
					else
					{
						$get_exp = Qry($conn,"SELECT id,name,exp_code,cap_amount FROM dairy.exp_head WHERE visible_to_supervisor='1' ORDER BY name ASC");
					}
					
					if(numRows($get_exp) > 0)
					{
						while($row_exp = fetchArray($get_exp))
						{
							echo "<option value='$row_exp[id]'>$row_exp[name]</option>";
						}
					}
					?>
				</select>
			</div>
			
			<input type="hidden" name="trip_id" value="<?php echo $id; ?>">
			
			<div class="form-group col-md-12">
				<label>Narration <font color="red"><sup>*</sup></font></label>
				<textarea class="form-control" oninput="this.value=this.value.replace(/[^A-Z a-z0-9,#.@/:;-]/,'');" name="narration" required="required"></textarea>
			</div>
		</div>
	

<script>
$('#modal_exp_btn')[0].click();	
$('#loadicon').fadeOut('slow');	
</script> 

