<?php
include("_header_datatable.php");
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Hisab Approvals : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
<?php
$qry = Qry($conn,"SELECT a.id,a.tno,a.trip_no,d.name as driver_name,a.branch,a.branch_user,e.name,a.timestamp FROM 
dairy.hisab_approval AS a 
LEFT OUTER JOIN emp_attendance AS e ON e.code = a.branch_user 
LEFT OUTER JOIN dairy.own_truck AS o ON o.tno = a.tno 
LEFT OUTER JOIN dairy.driver AS d ON d.code = o.driver_code 
WHERE a.supervisor='$_SESSION[supervisor_id]'");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
?>			  
	
				<div class="col-md-12 table-responsive" id="load_table_div">
                 <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#SN</th>
                        <th>Vehicle_No</th>
						<th>Trip_No</th>
						<th>Driver_Name</th>
						<th>Branch</th>
						<th>Branch_User</th>
						<th>Timestamp</th>
						<th>#Approve</th>
						<th>#Reject</th>
					  </tr>
                    </thead>
                    <tbody>
	<?php
	if(numRows($qry)==0)
	{
		echo "<tr>
			<td colspan='9'>No record found !</td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($qry))
		{
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			
			echo "<tr>
				<td>$i</td>
				<td>$row[tno]</td>
				<td>$row[trip_no]</td>
				<td>$row[driver_name]</td>
				<td>$row[branch]</td>
				<td>$row[name]</td>
				<td>$timestamp</td>
				<input type='hidden' id='trip_no_$row[id]' value='$row[trip_no]'>
				<input type='hidden' id='tno_$row[id]' value='$row[tno]'>
				<input type='hidden' id='req_branch_$row[id]' value='$row[branch]'>
				<input type='hidden' id='req_branch_user_$row[id]' value='$row[branch_user]'>
				<input type='hidden' id='hisab_req_timestamp_$row[id]' value='$row[timestamp]'>
				<td><button type='button' class='btn btn-xs btn-success' id='approve_btn_$row[id]' onclick='Approve($row[id])'>Approve</button></td>
				<td><button type='button' class='btn btn-xs btn-danger' id='reject_btn_$row[id]' onclick='Reject($row[id])'>Reject</button></td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("_footer_datatable.php") ?>

<div id="func_result"></div>  
<div id="func_result12"></div>  

<script>
function Approve(id)
{
	var tno = $('#tno_'+id).val();
	var trip_no = $('#trip_no_'+id).val();
	var req_branch = $('#req_branch_'+id).val();
	var req_branch_user = $('#req_branch_user_'+id).val();
	var hisab_req_timestamp = $('#hisab_req_timestamp_'+id).val();
	
	if(tno!='' && trip_no!='')
	{
		$('#approve_btn_'+id).attr('disabled',true);
		// $("#loadicon").show();
		jQuery.ajax({
			url: "./approve_hisab_save.php",
			data: 'id=' + id + '&tno=' + tno + '&trip_no=' + trip_no + '&req_branch=' + req_branch + '&req_branch_user=' + req_branch_user + '&hisab_req_timestamp=' + hisab_req_timestamp,
			type: "POST",
			success: function(data) {
				$("#func_result").html(data);
			},
			error: function() {}
		});
	}
	else
	{
		alert('Vehicle number OR trip number not found !');
	}
}

function Reject(id)
{
	$('#reject_btn_'+id).attr('disabled',true);
	jQuery.ajax({
			url: "./approve_hisab_reject.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data) {
				$("#func_result").html(data);
			},
			error: function() {}
		});
}
</script>