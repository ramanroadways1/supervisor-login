<?php
require_once("./_connect.php");
 
$timestamp = date("Y-m-d H:i:s");
$date = date("Y-m-d");

$trip_id = escapeString($conn,($_POST['trip_id']));
$amount = escapeString($conn,($_POST['amount']));
$qty = escapeString($conn,($_POST['qty']));
$rate = escapeString($conn,($_POST['rate']));
$date= escapeString($conn,($_POST['date']));
$branch= escapeString($conn,($_POST['branch']));
$narration = escapeString($conn,($_POST['narration']));

if($trip_id != $_SESSION['advance_cash_cng'])
{
	AlertErrorTopRight("Trip not verified !");
	exit();
}

$get_trip = Qry($conn,"SELECT branch,trip_no,driver_code,tno,lr_type FROM dairy.trip WHERE id='$trip_id'");

if(!$get_trip){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($get_trip)==0)
{
	AlertErrorTopRight("Trip not found !");
	exit();
}

$row_trip = fetchArray($get_trip);

$tno = $row_trip['tno'];
$trip_no = $row_trip['trip_no'];
$driver_code = $row_trip['driver_code'];

if(empty($driver_code))
{
	AlertErrorTopRight("Driver not found !");
	exit();
}

$check_scripts = Qry($conn,"SELECT id FROM dairy.running_scripts WHERE file_name!='LOAD_API_TRANS'");

if(!$check_scripts){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	exit();
}

if(numRows($check_scripts)>0)
{
	AlertErrorTopRight("Please try after some time !");
	exit();
}

$hisab_cache = Qry($conn,"SELECT id FROM dairy.hisab_cache WHERE tno='$tno'");

if(!$hisab_cache){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	exit();
}

if(numRows($hisab_cache)>0)
{
	AlertErrorTopRight("Vehicle hisab in process !");
	exit();
}

$trip_cache = Qry($conn,"SELECT id FROM dairy.trip_cache WHERE tno='$tno'");

if(!$trip_cache){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	exit();
}

if(numRows($trip_cache)>0)
{
	AlertErrorTopRight("Please try after some time !");
	exit();
}

$trans_id_Qry = GetTxnId_eDiary($conn,"DSL");
	
if(!$trans_id_Qry || $trans_id_Qry=="" || $trans_id_Qry=="0")
{
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
	
$trans_id = $trans_id_Qry;

StartCommit($conn);
$flag = true;	

$insert_cng = Qry($conn,"INSERT INTO dairy.diesel (trip_id,trip_no,trans_id,unq_id,tno,rate,qty,amount,date,cash_diesel,narration,branch,timestamp) VALUES 
('$trip_id','$trip_no','$trans_id','$diesel_key','$tno','$rate','$qty','$amount','$date','1','CASH_CNG','$branch','$timestamp')");

if(!$insert_cng){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$insert_cng2 = Qry($conn,"INSERT INTO dairy.diesel_entry (unq_id,tno,diesel,card_pump,card,dsl_company,narration,branch,date) VALUES 
('$diesel_key','$tno','$amount','CNG','CNG','CNG','CASH_CNG','$branch','$date')");

if(!$insert_cng2){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_trip = Qry($conn,"UPDATE dairy.trip SET diesel=diesel+'$amount',diesel_qty=ROUND(diesel_qty+'$qty',2) WHERE id='$trip_id'");

if(!$update_trip){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$select_amount = Qry($conn,"SELECT id,amount_hold FROM dairy.driver_up WHERE down=0 AND code='$driver_code' ORDER BY id DESC LIMIT 1");

if(!$select_amount){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$row_amount = fetchArray($select_amount);

$driver_bal_id = $row_amount['id'];
$hold_amt = $row_amount['amount_hold']-$amount;

$insert_book = Qry($conn,"INSERT INTO dairy.driver_book (driver_code,tno,trip_id,trip_no,trans_id,desct,debit,balance,date,branch,timestamp) VALUES 
('$driver_code','$tno','$trip_id','$trip_no','$trans_id','CASH_CNG','$amount','$hold_amt','$date','$branch','$timestamp')");

if(!$insert_book){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$update_hold_amount = Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold-'$amount' WHERE id='$driver_bal_id'");

if(!$update_hold_amount){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	AlertRightCornerSuccess("CNG Entry Success !");
	
	echo "<script>
			$('#CngForm')[0].reset();
		</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	exit();
}	
?>