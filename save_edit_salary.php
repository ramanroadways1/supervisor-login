<?php
require_once("./_connect.php");
 
$timestamp = date("Y-m-d H:i:s");
$date = date("Y-m-d");

$id = escapeString($conn,($_POST['id']));
$salary_array = escapeString($conn,($_POST['salary']));

$salary = explode("_",$salary_array)[0];
$salary_type = explode("_",$salary_array)[1];

if($salary_type=="1"){
	$sal_type = "$salary/trip";
}
else{
	$sal_type="$salary/month";
}	
			
$update = Qry($conn,"UPDATE dairy.driver_up SET salary_amount='$salary',salary_type='$salary_type' WHERE id='$id'");

if(!$update){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

echo "<script>
	$('#close_modal_btn')[0].click();
	$('#salary_td_$id').html('$sal_type');
</script>";

AlertRightCornerSuccess("Salary Updated !");
?>