<?php
include("_header_datatable.php");
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Current SALARY : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
<?php
$qry = Qry($conn,"SELECT t.id,t.tno,t.up,t.salary_amount,t.salary_type,t.branch,t.amount_hold,d.name as driver_name 
FROM dairy.driver_up AS t 
LEFT OUTER JOIN dairy.driver as d on d.code = t.code 
WHERE t.down=0 AND t.salary_approval='1' AND t.tno IN(SELECT tno FROM dairy.own_truck WHERE superv_id='$supv_id') ORDER BY t.up ASC");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
?>			  
	
				<div class="col-md-12 table-responsive" id="load_table_div">
                 <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#SN</th>
                        <th>Vehicle_No</th>
						<th>Driver_Name</th>
						<th>Branch</th>
						<th>Up_Date</th>
						<th>SALARY</th>
						<th>Balance</th>
						<th>Edit</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	if(numRows($qry)==0)
	{
		echo "<tr>
			<td colspan='8'>No record found !</td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($qry))
		{
			// $timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			
			$up_date=date("d-m-y", strtotime($row['up']));	
			
			if($row['salary_type']==1){
				$sal_type = "/trip";
			}
			else{
				$sal_type="/month";
			}	
			
			echo "<tr>
				<td>$i</td>
				<td>$row[tno]</td>
				<td>$row[driver_name]</td>
				<td>$row[branch]</td>
				<td>$up_date</td>
				<td id='salary_td_$row[id]'>$row[salary_amount] $sal_type</td>
				<td>$row[amount_hold]</td>
				<td><button type='button' class='btn btn-xs btn-danger' id='edit_btn_$row[id]' onclick='Edit($row[id])'>Edit</button></td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("_footer_datatable.php") ?>

<div id="func_result"></div>  
<div id="func_result12"></div>  

<script>
function Edit(id)
{
	$("#loadicon").show();
	jQuery.ajax({
		url: "./edit_driver_salary.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data) {
			$("#func_result12").html(data);
		},
		error: function() {}
	});
}
</script>