<?php
require_once("_connect.php");

$tno = escapeString($conn,($_POST['tno']));
$wheeler = escapeString($conn,($_POST['wheeler']));

$get_avg_data = Qry($conn,"SELECT id FROM dairy._avg_data WHERE veh_no='$tno'");

if(!$get_avg_data){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($get_avg_data) > 0)
{
	AlertErrorTopRight("Data already updated for this vehicle !");
	exit();
}

if($wheeler=='' || $wheeler==0)
{
	AlertErrorTopRight("Update wheeler first !");
	exit();
}
?>
<div class="row">
	<div class="form-group col-md-12" style="font-weight:bold;color:maroon">Route Type : Normal</div>

	<div class="form-group col-md-4">
		<label>Empty Avg. <sup><font color="red">*</font></sup></label>
		<input type="number" step="any" min="1" max="10" class="form-control" id="normal_empty_avg" />
	</div>
	
	<div class="form-group col-md-4">
		<label>Normal Avg. <sup><font color="red">*</font></sup></label>
		<input type="number" step="any" min="1" max="10" class="form-control" id="normal_normal_avg" />
	</div>
	
	<div class="form-group col-md-4">
		<label>Overload Avg. <sup><font color="red">*</font></sup></label>
		<input type="number" step="any" min="1" max="10" class="form-control" id="normal_ol_avg" />
	</div>
	
	<div class="form-group col-md-12" style="font-weight:bold;color:maroon">Route Type : Congested/Ghaat</div>

	<div class="form-group col-md-4">
		<label>Empty Avg. <sup><font color="red">*</font></sup></label>
		<input type="number" step="any" min="1" max="10" class="form-control" id="con_empty_avg" />
	</div>
	
	<div class="form-group col-md-4">
		<label>Normal Avg. <sup><font color="red">*</font></sup></label>
		<input type="number" step="any" min="1" max="10" class="form-control" id="con_normal_avg" />
	</div>
	
	<div class="form-group col-md-4">
		<label>Overload Avg. <sup><font color="red">*</font></sup></label>
		<input type="number" step="any" min="1" max="10" class="form-control" id="con_ol_avg" />
	</div>
	
	<div class="form-group col-md-12">
		<button type="button" onclick="SaveData()" class="btn btn-sm btn-success" id="update_btn"> <i class="fa fa-floppy-o" aria-hidden="true"></i> &nbsp; Save Data</button>
	</div>
</div>