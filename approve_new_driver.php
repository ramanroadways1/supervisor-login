<?php
include("_header_datatable.php");
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">NEW Driver Approval : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
<?php
$qry = Qry($conn,"SELECT * FROM dairy.driver_temp WHERE supervisor='$supv_id' AND (supervisor_timestamp IS NULL OR supervisor_timestamp=0)");

if(!$qry){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
?>			  
	
				<div class="col-md-12 table-responsive" id="load_table_div">
                 <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#SN</th>
						<th>Name</th>
						<th>Mobile</th>
						<th>LicenseNo</th>
						<th>AadharNo</th>
						<th>Timestamp</th>
						<th>Branch</th>
						<th>A/c details</th>
						<th>Driver_Photo</th>
						<th>License_Copy</th>
						<th>Aaadhar_Copy</th>
						<th>Approve</th>
						<th>Reject</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	if(numRows($qry)==0)
	{
		echo "<tr>
			<td colspan='13'>No record found !</td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($qry))
		{
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			
			if($row['ac_holder']!=''){
				$ac_details="Ac Holder: $row[ac_holder]<br>
				Ac No: $row[ac_no]<br>
				Bank: $row[bank]<br>
				Ifsc: $row[ifsc]";
			}
			else{
				$ac_details="";
			}
		
			echo "<tr>
				<td>$i</td>
				<td>$row[name]</td>
				<td>$row[mobile]</td>
				<td>$row[lic]</td>
				<td>$row[aadhar_no]</td>
				<td>$timestamp</td>
				<td>$row[branch]</td>
				<td>$ac_details</td>
			
			<input type='hidden' id='driver_name_$row[id]' value='$row[name]'>
			<input type='hidden' id='d_photo_$row[id]' value='$row[driver_photo]'>
			<input type='hidden' id='d_lic_front_$row[id]' value='$row[lic_front]'>
			<input type='hidden' id='d_lic_rear_$row[id]' value='$row[lic_rear]'>
			<input type='hidden' id='d_aadhar_front_$row[id]' value='$row[aadhar_front]'>
			<input type='hidden' id='d_aadhar_rear_$row[id]' value='$row[aadhar_rear]'>
			
			<td><button type='button' class='btn btn-xs btn-primary' onclick='ViewFile(\"PHOTO\",$row[id])'>View</button></td>
			<td>
				<button type='button' class='btn btn-xs btn-primary' onclick='ViewFile(\"LIC_FRONT\",$row[id])'>Front</button>
				&nbsp;
				<button type='button' class='btn btn-xs btn-primary' onclick='ViewFile(\"LIC_REAR\",$row[id])'>Rear</button>
			</td>
			<td>
				<button type='button' class='btn btn-xs btn-primary' onclick='ViewFile(\"AADHAR_FRONT\",$row[id])'>Front</button>
				&nbsp;
				<button type='button' class='btn btn-xs btn-primary' onclick='ViewFile(\"AADHAR_REAR\",$row[id])'>Rear</button>
			</td>
			
				<td><button type='button' class='btn btn-xs btn-success' id='approve_btn_$row[id]' onclick='Approve($row[id])'>Approve</button></td>
				<td><button type='button' class='btn btn-xs btn-danger' id='reject_btn_$row[id]' onclick='Reject($row[id])'>Reject</button></td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("_footer_datatable.php") ?>

<button type="button" id="ModalButton" style="display:none" class="btn btn-primary" data-toggle="modal" data-target="#myModal"></button>

<div class="modal fade" id="myModal" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-xl modal-dialog-centered">
    <div class="modal-content" style="max-height: calc(100vh - 70px);overflow: auto;">

      <div class="bg-primary modal-header">
        <h4 class="modal-title" style="font-size:13px;color:#FFF">Driver : <span style="color:" id="driver_name_html"></span></h4>
      </div>
	<div class="modal-body">
		<iframe class="responsive-iframe" src=""></iframe>
    </div>

      <div class="modal-footer">
        <button type="button" id="close_modal_button" class="btn btn-sm btn-danger" data-dismiss="modal">Close</button>
       </div>
	</div>
  </div>
</div>	

<div id="func_result"></div>  
<div id="func_result12"></div>  

<style>
.responsive-iframe {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
}

.modal-body {
  position: relative;
  overflow: hidden;
  width: 100%;
  padding-top: 56.25%; /* 16:9 Aspect Ratio (divide 9 by 16 = 0.5625) */
}
</style>

<script>
function ViewFile(type1,id)
{
	$("#loadicon").show();
	
	if(type1=='PHOTO')
	{
		var link1 = $('#d_photo_'+id).val();
	}
	else if(type1=='LIC_FRONT')
	{
		var link1 = $('#d_lic_front_'+id).val();
	}
	else if(type1=='LIC_REAR')
	{
		var link1 = $('#d_lic_rear_'+id).val();
	}
	else if(type1=='AADHAR_FRONT')
	{
		var link1 = $('#d_aadhar_front_'+id).val();
	}
	else if(type1=='AADHAR_REAR')
	{
		var link1 = $('#d_aadhar_rear_'+id).val();
	}
	
	var driver_name = $('#driver_name_'+id).val();
	$("#myModal iframe").attr("src", "../diary_admin/"+link1);
    $("#driver_name_html").html(driver_name);
    $("#ModalButton")[0].click();
	
	$("#loadicon").fadeOut('slow');
}

function Reject(id)
{
	// $("#loadicon").show();
	jQuery.ajax({
		url: "./reject_new_driver.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data) {
			$("#func_result12").html(data);
		},
		error: function() {}
	});
}
</script>

<script>
function Approve(id)
{
	// $("#loadicon").show();
	jQuery.ajax({
		url: "./save_approve_new_driver.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data) {
			$("#func_result").html(data);
		},
		error: function() {}
	});
}
</script>