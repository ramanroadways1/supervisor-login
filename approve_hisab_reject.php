<?php
require_once("./_connect.php");
 
$timestamp = date("Y-m-d H:i:s");
$date = date("Y-m-d");

$id = escapeString($conn,($_POST['id']));

StartCommit($conn);
$flag = true;

$update_3 = Qry($conn,"DELETE FROM dairy.hisab_approval WHERE id='$id'");

if(!$update_3){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	AlertRightCornerSuccess("Rejected Successfully !");
 
	echo "<script>
		$('#reject_btn_$id').attr('disabled',true);
		$('#reject_btn_$id').attr('onclick','');
		$('#reject_btn_$id').html('Approved');
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	echo "<script>
			$('#reject_btn_$id').attr('disabled',false);
		</script>";	
	exit();
}
?>