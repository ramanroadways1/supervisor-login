<?php
require_once("./_connect.php");
 
$timestamp = date("Y-m-d H:i:s");
$date = date("Y-m-d");

$trip_id = escapeString($conn,($_POST['trip_id']));
$amount = escapeString($conn,($_POST['amount']));
$exp_id = escapeString($conn,($_POST['exp_id']));
$narration = escapeString($conn,($_POST['narration']));

if($trip_id != $_SESSION['exp_trip_id'])
{
	AlertErrorTopRight("Trip not verified !");
	echo "<script>$('#exp_submit').attr('disabled',false);</script>";
	exit();
}

$get_trip = Qry($conn,"SELECT branch,trip_no,driver_code,tno,lr_type,fix_lane FROM dairy.trip WHERE id='$trip_id'");

if(!$get_trip){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#exp_submit').attr('disabled',false);</script>";
	exit();
}

if(numRows($get_trip)==0)
{
	AlertErrorTopRight("Trip not found !");
	echo "<script>$('#exp_submit').attr('disabled',false);</script>";
	exit();
}

$row_trip = fetchArray($get_trip);

$tno = $row_trip['tno'];
$trip_no = $row_trip['trip_no'];
$branch = $row_trip['branch'];
$driver_code = $row_trip['driver_code'];
$fix_lane = $row_trip['fix_lane'];

if(empty($driver_code))
{
	AlertErrorTopRight("Driver not found !");
	echo "<script>$('#exp_submit').attr('disabled',false);</script>";
	exit();
}

$get_expense = Qry($conn,"SELECT exp_code,name,visible_to_supervisor,lock_on_empty FROM dairy.exp_head WHERE id='$exp_id'");

if(!$get_expense){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>$('#exp_submit').attr('disabled',false);</script>";
	exit();
}

if(numRows($get_expense)==0)
{
	AlertErrorTopRight("Expense not found !");
	echo "<script>$('#exp_submit').attr('disabled',false);</script>";
	exit();
}

$row_exp = fetchArray($get_expense);

$exp_name = $row_exp['name'];
$exp_code = $row_exp['exp_code'];

$chk_exp_limit = Qry($conn,"SELECT exp_name,amount FROM dairy.trip_exp_limit WHERE exp='$exp_code' AND type='1' AND limit_for='SUPERVISOR' 
AND $amount>`amount`");

if(!$chk_exp_limit){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#exp_submit').attr('disabled',false);</script>";
	exit();
}

if(numRows($chk_exp_limit)>0)
{
	$row_exp_limit = fetchArray($chk_exp_limit);
	
	AlertErrorTopRight("Max limit allowed is: $row_exp_limit[amount] !");
	echo "<script>$('#exp_submit').attr('disabled',false);</script>";
	exit();
}

$can_entry_times = Qry($conn,"SELECT exp_name,entry_limit FROM dairy.trip_exp_limit WHERE exp='$exp_code' AND type='2' AND limit_for='SUPERVISOR'");

if(!$can_entry_times){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#exp_submit').attr('disabled',false);</script>";
	exit();
}

if(numRows($can_entry_times)>0)
{
	$row_times = fetchArray($can_entry_times);
	
	$fetch_total_entry_of_exp=Qry($conn,"SELECT id FROM dairy.trip_exp WHERE trip_id='$trip_id' AND exp_code='$exp_code'");
	
	if(!$fetch_total_entry_of_exp){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		AlertErrorTopRight("Error while processing request !");
		echo "<script>$('#exp_submit').attr('disabled',false);</script>";
		exit();
	}

	if(numRows($fetch_total_entry_of_exp)>=$row_times['entry_limit'])
	{
		AlertError("Allowed $row_times[entry_limit] times only !");
		echo "<script>$('#exp_submit').attr('disabled',false);</script>";
		exit();
	}
}

if($row_exp['visible_to_supervisor'] != "1")
{
	AlertErrorTopRight("Permission denied for the expense: $exp_name !");
	echo "<script>$('#exp_submit').attr('disabled',false);</script>";
	exit();
}

if($row_exp['lock_on_empty'] == "1" AND $row_trip['lr_type']=='EMPTY')
{ 
	AlertErrorTopRight("Expense: $row_exp[name] not allowed in EMPTY trip !");
	echo "<script>$('#exp_submit').attr('disabled',false);</script>";
	exit();
}

$check_scripts = Qry($conn,"SELECT id FROM dairy.running_scripts WHERE file_name!='LOAD_API_TRANS'");

if(!$check_scripts){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#exp_submit').attr('disabled',false);</script>";
	exit();
}

if(numRows($check_scripts)>0)
{
	AlertErrorTopRight("Please try after some time !");
	echo "<script>$('#exp_submit').attr('disabled',false);</script>";
	exit();
}

$hisab_cache = Qry($conn,"SELECT id FROM dairy.hisab_cache WHERE tno='$tno'");

if(!$hisab_cache){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#exp_submit').attr('disabled',false);</script>";
	exit();
}

if(numRows($hisab_cache)>0)
{
	AlertErrorTopRight("Vehicle hisab in process !");
	echo "<script>$('#exp_submit').attr('disabled',false);</script>";
	exit();
}

$trip_cache = Qry($conn,"SELECT id FROM dairy.trip_cache WHERE tno='$tno'");

if(!$trip_cache){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#exp_submit').attr('disabled',false);</script>";
	exit();
}

if(numRows($trip_cache)>0)
{
	AlertErrorTopRight("Please try after some time !");
	echo "<script>$('#exp_submit').attr('disabled',false);</script>";
	exit();
}

StartCommit($conn);
$flag = true;	

$trans_id_Qry = GetTxnId_eDiary($conn,"EXP");

if(!$trans_id_Qry || $trans_id_Qry=="" || $trans_id_Qry=="0"){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$trans_id = $trans_id_Qry;	

$insert_exp_diary = Qry($conn,"INSERT INTO dairy.trip_exp(trip_id,trans_id,tno,exp_name,exp_code,amount,date,narration,branch,timestamp) VALUES 
('$trip_id','$trans_id','$tno','$exp_name','$exp_code','$amount','$date','$narration','$branch','$timestamp')");

if(!$insert_exp_diary){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

if($exp_code=='TR00015')
{
	$toll_tax = $amount;
}
else
{
	$toll_tax = 0;
}

$update_trip = Qry($conn,"UPDATE dairy.trip SET expense=expense+'$amount',toll_tax=toll_tax+'$toll_tax' WHERE id='$trip_id'");

if(!$update_trip){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$select_amount = Qry($conn,"SELECT id,amount_hold FROM dairy.driver_up WHERE down=0 AND code='$driver_code' ORDER BY id DESC LIMIT 1");

if(!$select_amount){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$row_amount = fetchArray($select_amount);

$driver_bal_id = $row_amount['id'];
$hold_amt = $row_amount['amount_hold']-$amount;

$insert_book = Qry($conn,"INSERT INTO dairy.driver_book (driver_code,tno,trip_id,trip_no,trans_id,desct,debit,balance,date,branch,timestamp) VALUES 
('$driver_code','$tno','$trip_id','$trip_no','$trans_id','EXP-$exp_name','$amount','$hold_amt','$date','$branch','$timestamp')");

if(!$insert_book){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$update_hold_amount = Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold-'$amount' WHERE id='$driver_bal_id'");

if(!$update_hold_amount){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

// Insert alert if supervisor crossed the entry or amount limit start here

if($fix_lane!=0)
{
	$chk_count = Qry($conn,"SELECT id FROM dairy.trip_exp WHERE date='$date' AND tno='$tno'");
	
	if(!$chk_count){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$chk_amount = Qry($conn,"SELECT SUM(amount) as total_amount FROM dairy.trip_exp WHERE date='$date' AND tno='$tno'");
	
	if(!$chk_amount){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	// $row_chk_count = fetchArray($chk_count);
	$row_chk_amount = fetchArray($chk_amount);

	$get_count_limit = Qry($conn,"SELECT entry_limit FROM dairy.trip_adv_exp_limit_for_fix WHERE adv_exp!='ADV' AND limit_type='2'");
	
	if(!$get_count_limit){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($get_count_limit) > 0)
	{
		$row_entry_limit = fetchArray($get_count_limit);
		
		$total_entries = numRows($chk_count);
		
		if($total_entries > $row_entry_limit['entry_limit'])
		{
			$insert_alert_adv_entry_limit = Qry($conn,"INSERT INTO dairy._alert_fix_adv_exp(tno,driver,amount,entry_no,entry_limit,expense,
			supervisor,timestamp) VALUES ('$tno','$driver_code','$amount','$total_entries','$row_entry_limit[entry_limit]','$exp_name',
			'$supv_id','$timestamp')");
			
			if(!$insert_alert_adv_entry_limit){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
	}
	
	$get_amount_limit = Qry($conn,"SELECT amount FROM dairy.trip_adv_exp_limit_for_fix WHERE adv_exp!='ADV' AND limit_type='1'");
	
	if(!$get_amount_limit){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if(numRows($get_amount_limit) > 0)
	{
		$row_amount_limit = fetchArray($get_amount_limit);
		
		$total_amount = $row_chk_amount['total_amount'];
		
		if($total_amount > $row_amount_limit['amount'])
		{
			$insert_alert_adv_amount_limit = Qry($conn,"INSERT INTO dairy._alert_fix_adv_exp(tno,driver,amount,amount_limit,expense,supervisor,
			timestamp) VALUES ('$tno','$driver_code','$amount','$row_amount_limit[amount]','$exp_name','$supv_id','$timestamp')");
			
			if(!$insert_alert_adv_amount_limit){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
	}
}

// Insert alert if supervisor crossed the entry or amount limit end here

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	AlertRightCornerSuccess("Expense Added Successfully !");
	
	echo "<script>
			$('#Form2')[0].reset();
			$('#exp_submit').attr('disabled',false);
			$('#exp_modal_close_btn')[0].click();
		</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#exp_submit').attr('disabled',false);</script>";
	exit();
}	
?>