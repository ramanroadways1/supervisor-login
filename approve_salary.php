<?php
include("_header_datatable.php");
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">SALARY Approvals : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
<?php
$qry = Qry($conn,"SELECT d.id,d.code,d.tno,d.up,d.salary_amount,d.salary_type,d.branch,d.amount_hold,d2.name as driver_name,d2.last_salary,
(SELECT to_date FROM dairy.salary WHERE driver_code = d.code ORDER BY id DESC LIMIT 1) as sal_date_db
FROM dairy.driver_up AS d 
LEFT OUTER JOIN dairy.driver as d2 on d2.code = d.code 
WHERE d.salary_approval=0 AND d.tno IN(SELECT tno FROM dairy.own_truck WHERE superv_id='$supv_id')");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
?>			  
	
				<div class="col-md-12 table-responsive" id="load_table_div">
                 <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#SN</th>
                        <th>Vehicle_No</th>
						<th>Driver_Name</th>
						<th>Branch</th>
						<th>Up_Date</th>
						<th>SALARY</th>
						<th>Salary_Start_Date</th>
						<th>Opening_Balance</th>
						<th>Approve</th>
						<th>Edit</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	if(numRows($qry)==0)
	{
		echo "<tr>
			<td colspan='10'>No record found !</td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($qry))
		{
			// $timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			
			$up_date=date("d-m-y", strtotime($row['up']));	
			
			if($row['salary_type']==1){
				$sal_type = "/trip";
			}
			else{
				$sal_type="/month";
			}	
			
			if(!empty($row['sal_date_db'])){
				$sal_date=date("d-m-y", strtotime($row['sal_date_db']));		
			}
			else{								
				$sal_date=date("d-m-y", strtotime($row['last_salary']));		
			}
			
			echo "<tr>
				<td>$i</td>
				<td>$row[tno]</td>
				<td>$row[driver_name]</td>
				<td>$row[branch]</td>
				<td>$up_date</td>
				<td id='salary_td_$row[id]'>$row[salary_amount] $sal_type</td>
				<td>$sal_date</td>
				<td>$row[amount_hold]</td>
				<td><button type='button' class='btn btn-xs btn-success' id='approve_btn_$row[id]' onclick='Approve($row[id])'>Approve</button></td>
				<td><button type='button' class='btn btn-xs btn-danger' id='edit_btn_$row[id]' onclick='Edit($row[id])'>Edit</button></td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("_footer_datatable.php") ?>

<div id="func_result"></div>  
<div id="func_result12"></div>  

<script>
function Edit(id)
{
	$("#loadicon").show();
	jQuery.ajax({
		url: "./edit_driver_salary.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data) {
			$("#func_result12").html(data);
		},
		error: function() {}
	});
}
</script>

<script>
function Approve(id)
{
	// $("#loadicon").show();
	jQuery.ajax({
		url: "./approve_driver_salary.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data) {
			$("#func_result").html(data);
		},
		error: function() {}
	});
}
</script>