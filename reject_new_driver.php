<?php
require_once("./_connect.php");
 
$timestamp = date("Y-m-d H:i:s");
$date = date("Y-m-d");

$id = escapeString($conn,($_POST['id']));

$get_details = Qry($conn,"SELECT driver_photo,lic_front,lic_rear,aadhar_front,aadhar_rear FROM dairy.driver_temp WHERE id='$id'");

if(!$get_details){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($get_details)==0){
	AlertErrorTopRight("Driver not found !");
	exit();
}

$row = fetchArray($get_details);

unlink("../diary_admin/".$row['driver_photo']);
unlink("../diary_admin/".$row['lic_front']);
unlink("../diary_admin/".$row['lic_rear']);
unlink("../diary_admin/".$row['aadhar_front']);
unlink("../diary_admin/".$row['aadhar_rear']);

// AlertRightCornerSuccess("Approved Successfully !");
 
$delete_req = Qry($conn,"DELETE FROM dairy.driver_temp WHERE id='$id'");

if(!$delete_req){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
 
echo "<script>
	$('#approve_btn_$id').attr('disabled',true);
	$('#reject_btn_$id').html('Rejected');
	$('#reject_btn_$id').attr('disabled',true);
</script>";
exit();
?>