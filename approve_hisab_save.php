<?php
require_once("./_connect.php");
 
$timestamp = date("Y-m-d H:i:s");
$date = date("Y-m-d");

$id = escapeString($conn,($_POST['id']));
$tno = escapeString($conn,($_POST['tno']));
$trip_no = escapeString($conn,($_POST['trip_no']));
$req_branch = escapeString($conn,($_POST['req_branch']));
$req_branch_user = escapeString($conn,($_POST['req_branch_user']));
$hisab_req_timestamp = escapeString($conn,($_POST['hisab_req_timestamp']));

StartCommit($conn);
$flag = true;

$update_1 = Qry($conn,"UPDATE dairy.opening_closing SET hisab_req_branch='$req_branch',hisab_req_branch_user='$req_branch_user',
hisab_req_timestamp='$hisab_req_timestamp',trip_clear='1',trip_clear_timestamp='$timestamp' WHERE trip_no='$trip_no'");

if(!$update_1){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_2 = Qry($conn,"INSERT INTO dairy.hisab_approval_db(tno,trip_no,supervisor,supervisor_name,branch,branch_user,timestamp,approve_timestamp) 
SELECT tno,trip_no,supervisor,supervisor_name,branch,branch_user,timestamp,'$timestamp' FROM dairy.hisab_approval WHERE id='$id'");

if(!$update_2){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_3 = Qry($conn,"DELETE FROM dairy.hisab_approval WHERE id='$id'");

if(!$update_3){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	AlertRightCornerSuccess("Approved Successfully !");
 
	echo "<script>
		$('#approve_btn_$id').attr('disabled',true);
		$('#approve_btn_$id').attr('onclick','');
		$('#approve_btn_$id').html('Approved');
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	echo "<script>
			$('#approve_btn_$id').attr('disabled',false);
		</script>";	
	exit();
}
?>