<?php 
require_once './_connect.php';

$output ='';

$result = Qry($conn,"SELECT e.id,e.lrno,e.vou_no,e.adv_bal,e.lr_date,e.amount,e.narration,e.branch,e.username,e.admin_timestamp,e.timestamp,u.name 
	FROM allow_cash AS e 
	LEFT OUTER JOIN emp_attendance as u ON u.code = e.branch_user");	

if(!$result){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($result) == 0)
{
	echo "<script>
		alert('No result found !');
		window.close();
	</script>";
	exit();
}

 $output .= '
  <table border="1">    
    <tr>  
		<th>#</th>
                        <th>LR_number</th>
                        <th>LR_date</th>
                        <th>Vou_No</th>
                        <th>Adv/Bal</th>
                        <th>Amount</th>
                        <th>Branch</th>
                        <th>Username</th>
                        <th>Narration</th>
                        <th>Timestamp</th>
                        <th>Approved_By</th>
                        <th>Approved_At</th>
                       
	</tr>
  ';
 $i=1;
 
  while($row = fetchArray($result))
  {
		if($row['adv_bal']=="1"){
				$adv_bal="Advance";
			}else{
				$adv_bal="Balance";
			}
			
   $output .= '
    <tr> 
			<td>'.$i.'</td>
			<td>'.$row["lrno"].'</td>
			<td>'.$row["lr_date"].'</td>
			<td>'.$row["vou_no"].'</td>
			<td>'.$adv_bal.'</td>
			<td>'.$row["amount"].'</td>
			<td>'.$row["branch"].'</td>
			<td>'.$row["name"].'</td>
			<td>'.$row["narration"].'</td>
			<td>'.$row["timestamp"].'</td>
			<td>'.$row["username"].'</td>
			<td>'.$row["admin_timestamp"].'</td>
	</tr>
   ';
   $i++;
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=Allowed_Cash_in_FM.xls');
  echo $output;
?>