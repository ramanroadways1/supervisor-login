<?php
require_once("./_connect.php");
 
$timestamp = date("Y-m-d H:i:s");
$date = date("Y-m-d");

$tno = escapeString($conn,($_POST['tno']));
$wheeler = escapeString($conn,($_POST['wheeler']));
$n_empty = escapeString($conn,($_POST['normal_empty_avg']));
$n_loaded = escapeString($conn,($_POST['normal_normal_avg']));
$n_old = escapeString($conn,($_POST['normal_ol_avg']));
$c_empty = escapeString($conn,($_POST['con_empty_avg']));
$c_loaded = escapeString($conn,($_POST['con_normal_avg']));
$c_old = escapeString($conn,($_POST['con_ol_avg']));

if($tno=='' || $wheeler=='')
{
	AlertErrorTopRight("Vehicle or wheeler not found !");
	echo "<script>
		$('#loadicon').fadeOut('slow');
		$('#update_btn').attr('disabled',false);
	</script>";
	exit();
}

$select_max_cap = Qry($conn,"SELECT max_weight FROM max_weight_wheeler_wise WHERE wheeler='$wheeler'");

if(!$select_max_cap){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

$row_w = fetchArray($select_max_cap);

$max_capacity = $row_w['max_weight'];

$insert_avg_data1 = Qry($conn,"INSERT INTO dairy._avg_data(veh_no,wheeler,max_capacity,route_type,empty_avg,normal_avg,overload_avg,username,timestamp) 
VALUES ('$tno','$wheeler','$max_capacity','normal','$n_empty','$n_loaded','$n_old','$_SESSION[d_super]','$timestamp')");

if(!$insert_avg_data1){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

$insert_avg_data2 = Qry($conn,"INSERT INTO dairy._avg_data(veh_no,wheeler,max_capacity,route_type,empty_avg,normal_avg,overload_avg,username,timestamp) 
VALUES ('$tno','$wheeler','$max_capacity','congested','$c_empty','$c_loaded','$c_old','$_SESSION[d_super]','$timestamp')");

if(!$insert_avg_data2){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

// AlertRightCornerSuccess("Approved Successfully !");
 
echo "<script>
	Swal.fire({icon: 'success',html: '<font size=\'2\' color=\'black\'>Updated Success !</font>',});
	$('#loadicon').fadeOut('slow');
	$('#update_btn').attr('disabled',false);
</script>";
exit();
?>