<?php
include("_header.php");
?>
<script>
$(function() {
		$("#own_tno").autocomplete({
		source: './autofill/get_own_vehicle.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#own_tno').val(ui.item.value);   
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#own_tno').val("");   
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Vehicle does not exists.</font>',});
		}}, 
	focus: function (event, ui){
	return false;
	}
});});
</script>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Average Master : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				<div class="col-md-12">
					<div class="row">		

<script>
function ResetIp(elem)
{
	if(elem=='model')
	{
		$('#own_tno').val('');
	}
	else
	{
		$('#model').val('');
	}
}
</script>
						
						<div class="form-group col-md-3">
							<label>Vehicle Number <sup><font color="red">*</font></sup></label>
							<input id="own_tno" name="own_tno" autocomplete="off" required="required" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');ResetIp('tno')" 
							type="text" class="form-control" />
						</div>
						
						<div class="form-group col-md-1">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<label style="color:maroon"><span style="font-size:11px">-- OR --</span></label>
						</div>
						
						<div class="form-group col-md-3">
							<label>Select Model <sup><font color="red">*</font></sup></label>
							<select name="model" onchange="ResetIp('model')" id="model" class="form-control" required="required">
								<option value="">--select model--</option>
								<?php
								$get_model = Qry($conn,"SELECT model FROM dairy.model_list WHERE is_active='1' ORDER BY model ASC");
								
								while($row_model = fetchArray($get_model))
								{
									echo "<option value='$row_model[model]'>$row_model[model]</option>";
								}
								?>
							</select>
						</div>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" onclick="SearchVehicle()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn">
							<i id="search_icon" class="fa fa-search" aria-hidden="true"></i> 
							<i id="spinner_icon" style="display:none" class="fa fa-spinner fa-spin" aria-hidden="true"></i> &nbsp; Search</button>
						</div>
						
					<div style="overflow:auto" id="result_div" class="table-responsive form-group col-md-12">
					</div>					
				</div> 
				</div> 
                </div><!-- /.box-body --> 
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("_footer.php") ?>

<div id="func_result"></div>  

<script>
function SearchVehicle()
{
	var tno = $('#own_tno').val();
	var model = $('#model').val();
	
	if(tno=='' && model=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Please enter VehicleNo. or select Model first !</font>',});
	}
	else
	{
		$('#search_icon').hide();
		$('#spinner_icon').show();
		jQuery.ajax({
				url: "./_load_vehicle_for_avg_master.php",
				data: 'tno=' + tno + '&model=' + model,
				type: "POST",
				success: function(data) {
					$("#result_div").html(data);
				},
				error: function() {}
		});
	}
}
</script>
