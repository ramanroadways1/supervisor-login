<?php
include("_header.php");

$date = date("Y-m-d");

$get_trips = Qry($conn,"SELECT id FROM dairy.trip WHERE date(date)='$date' AND tno in(SELECT tno from dairy.own_truck WHERE superv_id='$_SESSION[supervisor_id]')");

$total_trips = numRows($get_trips);

$get_mb = Qry($conn,"SELECT id FROM mkt_bilty WHERE date='$date' AND tno in(SELECT tno from dairy.own_truck WHERE superv_id='$_SESSION[supervisor_id]')");

$total_mb = numRows($get_mb);

$hisab_today = Qry($conn,"SELECT id FROM dairy.log_hisab WHERE date(timestamp)='$date' AND tno in(SELECT tno from dairy.own_truck WHERE superv_id='$_SESSION[supervisor_id]')");

$hisab_today = numRows($hisab_today);

$free_vehicles = Qry($conn,"SELECT id FROM dairy.own_truck WHERE tno in(SELECT tno from dairy.own_truck WHERE is_sold!='1' AND superv_id='$_SESSION[supervisor_id]') 
AND tno NOT IN(SELECT tno FROM dairy.trip)");

$free_vehicles = numRows($free_vehicles);
?>

<div class="content-wrapper">
       <section class="content-header">
          <h1 style="font-size:16px;">Dashboard</h1>
       </section>

		<section class="content">
          <div class="row">
            <div onclick="GetData('trips_today')" class="col-lg-3 col-xs-6" style="cursor:pointer">
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3><?php echo $total_trips; ?></h3>
                  <p>Trips Today</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
              </div>
            </div>
            
			<div onclick="GetData('market_bilty')" class="col-lg-3 col-xs-6" style="cursor:pointer">
              <div class="small-box bg-green">
                <div class="inner">
                  <h3><?php echo $total_mb; ?></h3>
                  <p>Market Bilty Today</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
              </div>
            </div>
			
            <div onclick="GetData('hisab_today')" class="col-lg-3 col-xs-6" style="cursor:pointer">
              <div class="small-box bg-yellow">
                <div class="inner">
                 <h3><?php echo $hisab_today; ?></h3>
                  <p>Hisab Today</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
              </div>
            </div>
			
            <div onclick="GetData('free_vehicle')" class="col-lg-3 col-xs-6" style="cursor:pointer">
              <div class="small-box bg-red">
                <div class="inner">
                 <h3><?php echo $free_vehicles; ?></h3>
                   <p>Free Vehicles</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
              </div>
            </div>
          </div>
        </section><!-- /.content -->
      </div>
	  <!-- /.content-wrapper -->
     
<script>	 
function GetData(report_type)
{
	$('#loadicon').show();
		jQuery.ajax({
			url: "modal_show_report.php",
			data: 'report_type=' + report_type,
			type: "POST",
			success: function(data) {
			$("#load_modal_div").html(data);
			},
			error: function() {}
		});
}
</script>	

<div id="load_modal_div"></div> 
	 
<?php include("_footer.php"); ?>
