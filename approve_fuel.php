<?php
include("_header_datatable.php");
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Fuel Approvals : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
<?php
$qry = Qry($conn,"SELECT o.id,o.tno,o.diesel_tank_cap,o.diesel_left,o.qty_update_timestamp,e.name,d.name as driver_name 
FROM dairy.own_truck AS o 
LEFT OUTER JOIN rrpl_database.emp_attendance AS e ON e.code = o.qty_update_user 
LEFT OUTER JOIN dairy.driver AS d ON d.code = o.driver_code 
WHERE o.superv_id='$supv_id' AND diesel_trip_id=0 AND o.qty_update_timestamp IS NOT NULL AND o.supervisor_qty_approval_timestamp IS NULL");

if(!$qry){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
?>			  
	
				<div class="col-md-12 table-responsive" id="load_table_div">
                 <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#SN</th>
                        <th>Vehicle_No</th>
						<th>Driver_Name</th>
						<th>Tank Capacity</th>
						<th>Fuel Available</th>
						<th>Updated By</th>
						<th>Updated At</th>
						<th>Approve</th>
						<th>Reject</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	if(numRows($qry)==0)
	{
		echo "<tr>
			<td colspan='9'>No record found !</td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($qry))
		{
			$branch_timestamp = date("d-m-y h:i A",strtotime($row['qty_update_timestamp']));
			
			echo "<tr>
				<td>$i</td>
				<td>$row[tno]</td>
				<td>$row[driver_name]</td>
				<td>$row[diesel_tank_cap]</td>
				<td>$row[diesel_left]</td>
				<td>$row[name]</td>
				<td>$branch_timestamp</td>
				<td><button type='button' class='btn btn-xs btn-success' id='approve_btn_$row[id]' onclick='Approve($row[id])'>Approve</button></td>
				<td><button type='button' class='btn btn-xs btn-danger' id='reject_btn_$row[id]' onclick='Reject($row[id])'>Reject</button></td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("_footer_datatable.php") ?>

<div id="func_result"></div>  
<div id="func_result12"></div>  

<script>
function Reject(id)
{
	// $("#loadicon").show();
	jQuery.ajax({
		url: "./reject_fuel_approval.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data) {
			$("#func_result12").html(data);
		},
		error: function() {}
	});
}
</script>

<script>
function Approve(id)
{
	// $("#loadicon").show();
	jQuery.ajax({
		url: "./approve_fuel_save.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data) {
			$("#func_result").html(data);
		},
		error: function() {}
	});
}
</script>