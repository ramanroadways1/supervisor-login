<?php
require_once("_connect.php");

$date = date("Y-m-d");

$id = escapeString($conn,($_POST['id'])); 

if(empty($id)){
	AlertRightCornerError("Trip Id not found !");
	exit();
}

$qry = Qry($conn,"SELECT tno,branch,from_station,to_station,cash,fix_lane FROM dairy.trip WHERE id='$id'");
	
if(numRows($qry)==0)
{
	AlertRightCornerError("No record found !");
	exit();
}

$_SESSION['advance_trip_id'] = $id;

$row = fetchArray($qry);
?>
<div class="row">
			
			<div class="form-group col-md-6">
				<label>Amount <font color="red"><sup>*</sup></font></label>
				<input type="number" class="form-control" min="100" name="amount" required="required">
			</div>
			
			<div class="form-group col-md-6">
				<label>Branch <font color="red"><sup>*</sup></font></label>
				<select name="branch" class="form-control" required="required">
					<option value="">--select branch--</option>
					<?php
					$get_branch = Qry($conn,"SELECT username FROM user WHERE role='2' AND username NOT in('DUMMY','HEAD')");
					
					while($row_branch = fetchArray($get_branch))
					{
					?>
					<option <?php if($row_branch['username']==$row['branch']) { echo "selected"; } ?> value="<?php echo $row_branch['username']; ?>"><?php echo $row_branch['username']; ?></option>
					<?php					
					}
					?>
				</select>
			</div>
			
			<input type="hidden" name="trip_id" value="<?php echo $id; ?>">
			
			<div class="form-group col-md-12">
				<label>Narration <font color="red"><sup>*</sup></font></label>
				<textarea class="form-control" name="narration" oninput="this.value=this.value.replace(/[^A-Z a-z0-9,#.@/:;-]/,'');" required="required"></textarea>
			</div>
		</div>
	

<script>
$('#modal_adv_btn')[0].click();	
$('#loadicon').fadeOut('slow');	
</script> 
