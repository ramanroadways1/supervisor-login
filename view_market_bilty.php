<?php
include("_header.php");
?>

<script>
$(function() {
		$("#hisab_tno").autocomplete({
		source: 'autofill/get_own_vehicle.php',
		// appendTo: "#LaneRuleModal",
		select: function (event, ui) { 
              $('#hisab_tno').val(ui.item.value);   
             // $("#button1").attr('disabled',false);				   
             return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#hisab_tno").val('');
			$("#hisab_tno").focus();
			// $("#button1").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
});});
</script>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">VIEW Market Bilty : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12">
					<div class="row">
						<div class="form-group col-md-2">
							<label>Bilty Number</label>
							<input autocomplete="off" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');CheckInput('bilty_no');" type="text" class="form-control" id="bilty_no" />
						</div>
						
						<div class="form-group col-md-1">
							<label>&nbsp;</label>
							<?php if(!isMobile()) { echo "<br />"; } ?>
							<span style="font-size:12px;color:maroon">-- OR --</span>
						</div>
						
						<div class="form-group col-md-3">
							<label>Vehicle No.</label>
							<input autocomplete="off" type="text" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');CheckInput('tno');" class="form-control" id="hisab_tno" />
						</div>
						
						<div class="form-group col-md-1">
							<label>&nbsp;</label>
							<?php if(!isMobile()) { echo "<br />"; } ?>
							<span style="font-size:12px;color:maroon">-- AND/OR --</span>
						</div>
						
						<div class="form-group col-md-3">
							<label>Date range.</label>
							<select style="font-size:12px" name="duration" id="duration" class="form-control" required>
								<option style="font-size:12px" value="">---SELECT---</option>
								<option style="font-size:12px" value="-0 days">Today's</option>
								<option style="font-size:12px" value="-1 days">Last 2 days</option>
								<option style="font-size:12px" value="-4 days">Last 5 days</option>
								<option style="font-size:12px" value="-6 days">Last 7 days</option>
								<option style="font-size:12px" value="-9 days">Last 10 days</option>
								<option style="font-size:12px" value="-14 days">Last 15 days</option>
								<option style="font-size:12px" value="-29 days">Last 30 days</option>
								<option style="font-size:12px" value="-59 days">Last 60 days</option>
								<option style="font-size:12px" value="-89 days">Last 90 days</option>
								<option style="font-size:12px" value="-119 days">Last 120 days</option>
								<option style="font-size:12px" value="FULL">FULL REPORT</option>
							</select>
						</div>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" style="margin-top:1px" onclick="SearchBilty()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-search" aria-hidden="true"></i> &nbsp; Search Mkt Bilty</button>
						</div>
					</div>
				</div>
				
<script>				
function CheckInput(elem)
{
	if(elem=='bilty_no')
	{
		$('#hisab_tno').val('');
		$('#duration').val('');
	}
	else
	{
		$('#bilty_no').val('');
	}
}
</script>				
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12 table-responsive" id="load_table">
              
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
<div id="load_modal_result"></div>  

<script>	
function SearchBilty()
{
	var tno = $('#hisab_tno').val();
	var bilty_no = $('#bilty_no').val();
	var duration = $('#duration').val();
	
	if(tno=='' && bilty_no=='' && duration=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Atleast one field is required !</font>',});
	}
	else
	{
		$('#loadicon').show();
			jQuery.ajax({
				url: "_load_market_bilty.php",
				data: 'tno=' + tno + '&bilty_no=' + bilty_no + '&duration=' + duration,
				type: "POST",
				success: function(data) {
					$("#load_table").html(data);
					$('#example').DataTable({ 
						"destroy": true, //use for reinitialize datatable
					});
				},
				error: function() {}
		});
	}
}

function ViewBilty(id)
{
	$('#loadicon').show();
	jQuery.ajax({
		url: "modal_bilty_load_by_id.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data) {
			$("#load_modal_result").html(data);
		},
		error: function() {}
		});
}
</script>

<?php include("_footer.php") ?>