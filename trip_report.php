<?php
include("_header.php");
?>

<script>
$(function() {
		$("#tno").autocomplete({
		source: 'autofill/get_own_vehicle.php',
		// appendTo: "#LaneRuleModal",
		select: function (event, ui) { 
              $('#tno').val(ui.item.value);   
             // $("#button1").attr('disabled',false);				   
             return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle Number does not exists.');
			$("#tno").val('');
			$("#tno").focus();
			// $("#button1").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
});});
</script>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Trip Report : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12">
					<div class="row">
						<div class="form-group col-md-3">
							<label>Vehicle No. <font color="red"><sup>*</sup></font></label>
							<input autocomplete="off" type="text" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');" class="form-control" id="tno" />
						</div>
						
						<div class="form-group col-md-3">
							<label>From Date <font color="red"><sup>*</sup></font></label>
							<input id="from_date" name="from_date" type="date" class="form-control" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
						</div>
						
						<div class="form-group col-md-3">
							<label>To Date <font color="red"><sup>*</sup></font></label>
							<input id="to_date" name="to_date" type="date" class="form-control" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
						</div>
						
						<div class="form-group col-md-3">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" style="margin-top:1px" onclick="SearhResult()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn"><i class="fa fa-search" aria-hidden="true"></i> &nbsp; Search </button>
							&nbsp; <span style="color:maroon;font-size:12px">--OR--</span> &nbsp; 
							<button type="button" style="margin-top:1px" onclick="DownloadTripReport()" class="btn btn-sm btn-primary <?php if(isMobile()) { echo "btn-block"; } ?>"><i class="fa fa-download" aria-hidden="true"></i> &nbsp; Download </button>
						</div>
					</div>
				</div>
					
				<div class="col-md-12">&nbsp;</div>
				
				<div class="col-md-12 table-responsive" id="load_table">
              
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div id="func_result"></div>  
<div id="load_modal_result"></div>  

<script>	
function SearhResult()
{
	var tno = $('#tno').val();
	var from_date = $('#from_date').val();
	var to_date = $('#to_date').val(); 
	
	if(tno=='' || from_date=='' || to_date=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Fill all fields first !</font>',});
	}
	else
	{
		$('#loadicon').show();
			jQuery.ajax({
				url: "_load_trip_report.php",
				data: 'tno=' + tno + '&from_date=' + from_date + '&to_date=' + to_date,
				type: "POST",
				success: function(data) {
					$("#load_table").html(data);
					$('#example').DataTable({ 
						"destroy": true, //use for reinitialize datatable
					});
				},
				error: function() {}
		});
	}
}

function DownloadTripReport()
{
	var tno = $('#tno').val();
	var from_date = $('#from_date').val();
	var to_date = $('#to_date').val(); 
	
	if(tno=='' || from_date=='' || to_date=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Fill all fields first !</font>',});
	}
	else
	{
		$('#tno_form1').val(tno);
		$('#from_date_form1').val(from_date);
		$('#to_date_form1').val(to_date);
		
		$('#FormExcel1')[0].submit();
	}
}
</script>

<form action="download_excel.php" id="FormExcel1" target="_blank" method="POST">
	<input type="hidden" name="tno" id="tno_form1">
	<input type="hidden" name="from_date" id="from_date_form1">
	<input type="hidden" name="to_date" id="to_date_form1">
</form>

<?php include("_footer.php") ?>