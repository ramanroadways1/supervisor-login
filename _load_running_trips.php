<?php
require_once("_connect.php");

$tno = escapeString($conn,($_POST['tno']));

$get_trips = Qry($conn,"SELECT t.id,t.trip_no,t.branch,t.from_station,t.to_station,t.lr_type,t.lrno,t.date,t.end_date,t.cash,t.rtgs,t.supervisor_approval,
t.expense,d.name as driver_name 
FROM dairy.trip AS t
LEFT OUTER JOIN dairy.driver AS d ON d.code = t.driver_code
WHERE t.tno='$tno' ORDER BY t.id ASC");

$driver_name="";
$trip_no="";
		
if(!$get_trips){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
?>
  <table id="example1" class="table table-bordered table-striped">
					<thead>
                      <tr class="bg-default"><td style="font-weight:bold" colspan="14" id="first_row"></td></tr>
					  
                      <tr>
                        <th>#</th>
                        <th>#Status</th>
                        <th>#View</th>
                        <th>Branch</th>
                        <th>From</th>
                        <th>To</th>
                        <th>LR_Type</th>
                        <th>LR_No</th>
                        <th>Trip_Date</th>
                        <th>End_Date</th>
                        <th>Advance</th>
                        <th>Expenses</th>
                        <th>#Add_Adv</th>
                        <th>#Add_Exp</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	if(numRows($get_trips)==0)
	{
		echo "<tr>
			<td colspan='14'>No user found !</td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($get_trips))
		{
			$driver_name = $row['driver_name'];
			$trip_no = $row['trip_no'];
		
			$trip_date = date("d-m-y",strtotime($row['date']));
			
			if($row['end_date']==0 || $row['end_date']==''){
				$end_date = "<font color='green'>Running</font>";
			}else{
				$end_date = date("d-m-y",strtotime($row['end_date']));
			}
			
			if($row['rtgs']>0){
				$advance = "Cash/ATM: $row[cash]<br>RTSG: $row[rtgs]";
			}
			else{
				$advance = "Cash/ATM: $row[cash]";
			}
			
			if($row['supervisor_approval']=="1"){
				$supervisor_approval = "<button class='btn btn-xs btn-success' onclick='AllowTrip($row[id])' type='button'><i class='fa fa-unlock' aria-hidden='true'></i> Allow</button>";
			}else{
				$supervisor_approval = "<button class='btn btn-xs btn-danger' onclick='BlockTrip($row[id])' type='button'><i class='fa fa-lock' aria-hidden='true'></i> Block</button>";
			}
			
			echo "<tr>
				<td>$i</td>
				<td>$supervisor_approval</td>
				<td><button class='btn btn-xs btn-primary' onclick='ViewTrip($row[id])' type='button'><i class='fa fa-street-view' aria-hidden='true'></i> View Trip</button></td>
				<td>$row[branch]</td>
				<td>$row[from_station]</td>
				<td>$row[to_station]</td>
				<td>$row[lr_type]</td>
				<td>$row[lrno]</td>
				<td>$trip_date</td>
				<td>$end_date</td>
				<td>$advance</td>
				<td>$row[expense]</td>
				<td><button class='btn btn-xs btn-primary' onclick='AdvanceEntry($row[id])' type='button'><i class='fa fa-credit-card' aria-hidden='true'></i> Adv.Entry</button></td>
				<td><button class='btn btn-xs btn-primary' onclick='ExpenseEntry($row[id])' type='button'><i class='fa fa-inr' aria-hidden='true'></i> Exp.Entry</button></td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				  
<script>
$(document).ready(function() {
    $('#example1').DataTable();
} );

$('#search_icon').show();
$('#spinner_icon').hide();
</script>

<?php
$first_row_data = "Vehicle_No: $tno, Trip_No: $trip_no, Driver: $driver_name";

echo "<script>
	$('#first_row').html('$first_row_data');
</script>";
?>				  