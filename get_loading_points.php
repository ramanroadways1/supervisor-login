<?php
include("_header_datatable.php");
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Address Book Loading Points : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
<?php
$qry = Qry($conn,"SELECT a.id,a.code,a.label,a._lat,a._long,a.google_km,a.pincode,a.branch,a.record_by,a.tno_visited,a.visit_date,a.timestamp,s.name as location,
u.name as username,p.name as party,p.gst 
FROM address_book_consignor AS a 
LEFT OUTER JOIN station AS s ON s.id = a.from_id 
LEFT OUTER JOIN consignor AS p ON p.id = a.consignor 
LEFT OUTER JOIN emp_attendance AS u ON u.code = a.branch_user 
WHERE a.admin_update_timestamp IS NULL ORDER BY a.id ASC");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
?>			  
	
				<div class="col-md-12 table-responsive" id="load_table_div">
                 <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#Code</th>
                        <th>Label</th>
                        <th>Source & Consignor</th>
						<th>Coordinates</th>
						<th>Pincode</th>
                        <th>Distance</th>
                        <th>Branch & User</th>
                        <th>Record Details</th>
                        <th>Timestamp</th>
                        <th>#Edit#</th>
                        <th>#Delete#</th>
						<th>#MarkOK#</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	if(numRows($qry)==0)
	{
		echo "<tr>
			<td colspan='12'>No record found !</td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($qry))
		{
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			
			if($row['tno_visited']!=''){
				$visit_date = "<br>(".date("d-m-y",strtotime($row['visit_date'])).")";
			}else{
				$visit_date="";
			}
			echo "<tr>
				<td>$row[code]</td>
				<td id='label_col_$row[id]'>$row[label]</td>
				<td>$row[location]-><br>$row[party]<br>($row[gst])</td>
				<td><a id='coordinates_row_$row[id]' href='https://www.google.com/maps/place/$row[_lat],$row[_long]' target='_blank'><button class='btn btn-xs btn-warning'>View GMap</button></a></td>
				<td id='pincode_row_$row[id]'>$row[pincode]</td>
				<td>$row[google_km]</td>
				<td>$row[branch]<br>($row[username])</td>
				<td>$row[record_by]<br>$row[tno_visited]$visit_date</td>
				<td>$timestamp</td>
				<td><button onclick='Edit($row[id])'  id='btn_edit_$row[id]' class='btn btn-xs btn-primary'>Edit</button></td>
				<td><button onclick='Delete($row[id])'  id='btn_delete_$row[id]' class='btn btn-xs btn-danger'>Delete</button></td>
				<td><button onclick='Approve($row[id])' id='btn_approve_$row[id]' class='btn btn-xs btn-success'>Approve</button></td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("_footer_datatable.php") ?>

<div id="func_result"></div>  
<div id="func_result12"></div>  

<script>
function Edit(id)
{
	$("#loadicon").show();
	jQuery.ajax({
		url: "./edit_modal_poi.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data) {
			$("#func_result12").html(data);
		},
		error: function() {}
	});
}
</script>

<script>
function Delete(id)
{
	$("#loadicon").show();
	jQuery.ajax({
		url: "./delete_modal_poi.php",
		data: 'id=' + id + '&type=' + 'loading',
		type: "POST",
		success: function(data) {
			$("#func_result").html(data);
		},
		error: function() {}
	});
}

function Approve(id)
{
	// $("#loadicon").show();
	jQuery.ajax({
		url: "./approve_poi.php",
		data: 'id=' + id + '&type=' + 'loading',
		type: "POST",
		success: function(data) {
			$("#func_result").html(data);
		},
		error: function() {}
	});
}
</script>