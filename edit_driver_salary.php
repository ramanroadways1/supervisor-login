<?php
require_once '_connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$id = escapeString($conn,$_POST['id']);

$qry = Qry($conn,"SELECT t.tno,t.salary_amount,t.salary_type,o.model 
FROM dairy.driver_up AS t 
LEFT OUTER JOIN dairy.own_truck AS o ON o.tno = t.tno 
WHERE t.id='$id'");
	
if(!$qry){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

$row = fetchArray($qry);
?>
<button type="button" id="ModalButton" style="display:none" class="btn btn-primary" data-toggle="modal" data-target="#Modal1"></button>

<form id="EditSalaryForm" autocomplete="off">
<div class="modal" id="Modal1" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header bg-primary">
		<h4 style="font-size:15px;" class="modal-title">Edit Salary : <?php echo $row['tno']; ?></h4>
      </div>
	 <div class="modal-body">
	 
		 <div class="row">
			
			<div class="form-group col-md-12">
				<label>Select Salary <sup><font color="red">*</font></sup></label>
				<select class="form-control" required="required" name="salary">
				<option value="">--select salary--</option>
				<?php
				$qry1 = Qry($conn,"SELECT model,salary,type FROM dairy.salary_master WHERE model = '$row[model]'");
				
				while($row1 = fetchArray($qry1))
				{
					if($row1['type']=="1"){
						?>
					<option <?php if($row['salary_amount']==$row1['salary'] AND $row['salary_type']==$row1['type']) { echo "selected"; } ?> value="<?php echo $row1['salary']."_".$row1['type'] ?>">	<?php echo $row1['salary']; ?> / trip</option>
						<?php
					}else {
						?>
					<option <?php if($row['salary_amount']==$row1['salary'] AND $row['salary_type']==$row1['type']) { echo "selected"; } ?> value="<?php echo $row1['salary']."_".$row1['type'] ?>">	<?php echo $row1['salary']; ?> / month</option>
						<?php	
					}
				} 
				?>
				</select>
			</div>
			
		</div>
			<input type="hidden" name="id" value="<?php echo $id; ?>">	
	  </div>

      <div class="modal-footer">
        <button type="submit" id="update_button" class="btn btn-sm btn-primary">Update</button>
        <button type="button" id="close_modal_btn" class="btn btn-sm btn-danger" data-dismiss="modal">Close</button>
      </div>
	</form>
    </div>
  </div>
</div>	

<script type="text/javascript">
$(document).ready(function (e) {
$("#EditSalaryForm").on('submit',(function(e) {
$("#loadicon").show();
$("#update_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_edit_salary.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#func_result").html(data);
		$("#update_button").attr("disabled", false);
	},
	error: function() 
	{} });}));});
</script>

<script>
	$('#ModalButton')[0].click();
	$('#loadicon').fadeOut('slow');
</script>